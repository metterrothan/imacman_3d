#pragma once

#include <iostream>

#define PI 3.1415926535897

namespace glimac {

    class FreeflyCamera2 {
        private:
            glm::vec3 m_Position;

            float m_fPhi;
            float m_fTheta;

            glm::vec3 m_FrontVector;
            glm::vec3 m_LeftVector;
            glm::vec3 m_UpVector;

        public:
            FreeflyCamera2(glm::vec3 pos = glm::vec3(0, 0, 5), float phi = PI, float theta = 0.f){
                m_Position = pos;
                m_fPhi = phi;
                m_fTheta = theta;

                computeDirectionVectors();
            }

            void computeDirectionVectors(){
                m_FrontVector = glm::vec3(cos(m_fTheta)*sin(m_fPhi), sin(m_fTheta), cos(m_fTheta)*cos(m_fPhi));
                m_LeftVector = glm::vec3(sin(m_fPhi + PI / 2), 0, cos(m_fPhi + PI / 2));
                m_UpVector = glm::cross(m_FrontVector, m_LeftVector);
            }

            void moveLeft(float t){
                m_Position += t * m_LeftVector;
            }

            void moveFront(float t){
                m_Position += t * m_FrontVector;
            }

            void rotateLeft(float degrees){
                m_fPhi += glm::radians(degrees);
                computeDirectionVectors();
            }

            void rotateUp(float degrees){
                m_fTheta += glm::radians(degrees);
                computeDirectionVectors();
            }

            void setPosition(const float x, const float y, const float z){
                m_Position.x = x;
                m_Position.y = y;
                m_Position.z = z;
                computeDirectionVectors();
            }

            void setPosition(const glm::vec3 & pos){
                m_Position.x = pos.x;
                m_Position.y = pos.y;
                m_Position.z = pos.z;
                computeDirectionVectors();
            }

            glm::mat4 getViewMatrix() const {
                return glm::lookAt(m_Position, m_Position+m_FrontVector, m_UpVector);
            }

            inline const glm::vec3 getPosition() const { return m_Position; }
    };
}
