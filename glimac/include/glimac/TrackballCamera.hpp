#pragma once

#include <iostream>

namespace glimac {

    class TrackballCamera2 {
        public:
            TrackballCamera2(float distance = -1.0f, float angleX = 0.0f, float angleY = 0.0f) {
                m_fDistance = distance;
                m_fAngleX = angleX;
                m_fAngleY = angleY;
            }

            inline void setDistance(const float distance){ m_fDistance = distance; }
            inline void setAngleX(const float angle){ m_fAngleX = angle; }
            inline void setAngleY(const float angle){ m_fAngleY = angle; }

            inline float getDistance() const{ return m_fDistance; }
            inline float getAngleX() const{ return m_fAngleX; }
            inline float getAngleY() const{ return m_fAngleY; }

            void moveFront(float delta){
                m_fDistance += delta;
            }

            void rotateLeft(float degrees){
                m_fAngleY += degrees;
            }

            void rotateUp(float degrees){
                m_fAngleX += degrees;
            }

            glm::mat4 getViewMatrix() const {
                glm::mat4 mat = glm::mat4(1.0f);
                
                mat = glm::translate(mat, glm::vec3(0, 0, m_fDistance));
                mat = glm::rotate(mat, glm::radians(m_fAngleX), glm::vec3(1, 0, 0));
                mat = glm::rotate(mat, glm::radians(m_fAngleY), glm::vec3(0, 1, 0));

                return mat;
            }

        private:
            float m_fDistance; // distance par rapport au centre de la scène
            float m_fAngleX; // angle caméra axe x
            float m_fAngleY; // angle caméra axe y
    };
}
