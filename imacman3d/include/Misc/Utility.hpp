#ifndef UTILITY_HPP
#define UTILITY_HPP

#ifndef NDEBUG
#define LOG_MSG(...) std::cout << (__VA_ARGS__) << std::endl;
#else
#define LOG_MSG(...)
#endif

#include <iostream>
#include <fstream>
#include <vector>
#include <glimac/glm.hpp>

namespace utility {
    template <typename T> static T clamp(T val, const T min, const T max){
        assert(min < max);
        if(val < min) val = min;
        if(val > max) val = max;
        return val;
    }
    /**
    *   @brief  Linear interpolation function
    *   @param  a - current value
     *  @param b - next value
     *  @param f - delta
    *   @return Interpolated value
    */
    static inline float lerp(const float a, const float b, const float f){
        return (1 - f) * a + f * b;
    }

    static inline double lerp(const double a, const double b, const double f){
        return (1 - f) * a + f * b;
    }

    static inline glm::vec3 lerp(const glm::vec3 & a, const glm::vec3 & b, const float f){
        glm::vec3 temp;

        temp.x = lerp(a.x, b.x, f);
        temp.y = lerp(a.y, b.y, f);
        temp.z = lerp(a.z, b.z, f);

        return temp;
    }

    /**
    *   @brief  Checks if a file exits
    *   @param  path
    *   @return bool
    */
    static bool file_exists(const std::string path) {
        std::ifstream file(path.c_str());
        return file.good();
    }

    static int random_int(int a, int b){
        assert(a < b);
        return rand() % (b - a + 1) + a;
    }

    static float random_float(float a, float b){
        assert(a < b);
        return ((b - a) * ((float)rand() / RAND_MAX)) + a;
    }

    /*
    * Split a string using a delimiter
    * @param haystack
    * @param delimiter
    * @return list of sub-elements
    */
    static std::vector<std::string> split_str(std::string haystack, const std::string delimiter){
        std::vector<std::string> list;
        size_t pos = 0;

        std::string token;
        while((pos = haystack.find(delimiter)) != std::string::npos){
            token = haystack.substr(0, pos);
            list.push_back(token);
            haystack.erase(0, pos + delimiter.length());
        }
        return list;
    }
};

#endif //UTILITY_HPP
