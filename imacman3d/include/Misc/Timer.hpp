//
// Created by Jeremie on 30-Dec-17.
//

#ifndef TIMER_HPP
#define TIMER_HPP

#include <iostream>
#include <chrono>

using namespace std::chrono_literals;
using hrclock = std::chrono::high_resolution_clock;

class Timer
{
private:
    std::chrono::nanoseconds m_duration = 0ns;
    hrclock::time_point m_last = hrclock::now();
    bool m_paused = false;

public:
    Timer(){ }

    template <typename T, typename U> T getDuration(){
        auto diff = std::chrono::duration_cast<U>(m_duration);
        return (T)diff.count();
    }

    void pause(){
        m_paused = true;
    }

    void resume(){
        m_last = hrclock::now();
        m_paused = false;
    }

    void reset(){
        m_duration = 0ns;
        m_last = hrclock::now();
    }

    void update(){
        if(!m_paused){
            m_duration += hrclock::now() - m_last;
            m_last = hrclock::now();
        }
    }
};

#endif //TIMER_HPP
