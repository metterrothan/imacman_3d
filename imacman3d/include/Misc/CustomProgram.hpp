#ifndef TEST_HPP
#define TEST_HPP

#define GLEW_STATIC

#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <glimac/Image.hpp>
#include <glimac/Program.hpp>

using namespace glimac;

struct CustomProgram
{
    Program m_program;

    CustomProgram(){
        m_program = loadProgram("./shaders/cube.vs.glsl", "./shaders/cube.fs.glsl");
    }

    const GLuint getGLId(){
        return m_program.getGLId();
    }

    void use(){
        m_program.use();
    }
};

#endif //TEST_HPP
