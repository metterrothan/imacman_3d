#ifndef STATE_H
#define STATE_H

#include <iostream>
#include <SDL/SDL.h>
#include <GL/glew.h>
#include <glimac/glm.hpp>

#include "StateFactory.hpp"
#include "GameState/GameState.hpp"
#include "Misc/Utility.hpp"

class StateController;

class State
{
protected:
    glm::ivec2 m_mouse_pos;

    StateController * m_controller = nullptr;

public:
    virtual void init(StateController * controller){
        m_controller = controller;
    }

    virtual void update() = 0;
    virtual void render(double) = 0;

    /**
     * Handles keyboard input
     * @param key - Key code
     * @param active - Key state (pressed/released)
     */
    virtual void keyPressed(const uint32_t key, const bool active) = 0;

    /**
     * Handles mouse click events
     * @param type - Button
     * @param active - Mouse state (pressed/released)
     */
    virtual void mousePressed(const uint32_t type, const bool active) = 0;

    /**
     * Handles mouse move
     * @param dir - Current mouse pos
     * @param dir - Current mouse direction vector
     */
    virtual void mouseMove(const glm::ivec2 & dir) = 0;

    /**
     * Handles mouse move
     * @param pos - Current mouse position
     */
    void setMousePos(const glm::ivec2 & pos){
        m_mouse_pos = pos;
    }

    virtual void stop() = 0;

    virtual ~State(){ }
};

#endif //STATE_H
