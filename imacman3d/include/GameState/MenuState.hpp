#ifndef MENUSTATE_HPP
#define MENUSTATE_HPP

#define GLEW_STATIC

#include "StateFactory.hpp"
#include "GameState/State.hpp"

class MenuState : public State
{
public :
    void init(StateController *);
    void update();
    void render(double);
    void keyPressed(const uint32_t, const bool);
    void mousePressed(const uint32_t, const bool);
    void mouseMove(const glm::ivec2 &);
    void stop();

    ~MenuState();

private:
    //GLuint m_texture[NBR_TEXTURES];
    //GLuint m_program;

    //MenuAccueil * m_accueil;
};

#endif //MENUSTATE_HPP
