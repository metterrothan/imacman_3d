#ifndef LEVELSTATE_HPP
#define LEVELSTATE_HPP

#include "Game.hpp"
#include "StateFactory.hpp"
#include "GameState/State.hpp"
#include "Misc/Timer.hpp"

class Scene;
class Player;

class LevelState : public State
{
private:
    Timer m_timer;
    bool m_paused = false;
    bool m_mouse_pressed = false;
    unsigned int m_level = 1;
    Scene * m_scene;

public :
    void init(StateController *);
    void pause();
    void resume();
    void update();
    void render(double);
    void keyPressed(const uint32_t, const bool);
    void mousePressed(const uint32_t, const bool);
    void mouseMove(const glm::ivec2 &);
    void stop();
    void loadLevel(const unsigned int);

    inline const bool isPaused() const{ return m_paused; }

    ~LevelState();
};

#endif //LEVELSTATE_HPP
