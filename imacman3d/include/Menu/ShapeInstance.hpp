
#ifndef IMACGL_SHAPEINSTANCE_HPP
#define IMACGL_SHAPEINSTANCE_HPP


#define GLEW_STATIC
#include <iostream>
#include <fstream>
#include <GL/glew.h>
#include <GL/glut.h>
#include <memory>
#include <SDL/SDL.h>
#include <glimac/SDLWindowManager.hpp>
#include <glimac/Program.hpp>
#include <glimac/common.hpp>
#include <glimac/Sphere.hpp>
#include <glimac/Image.hpp>
#include <string>
#include <SDL/SDL_image.h>
#include "Shape.hpp"
#include <vector>

#define POSITION_LOCATION 0
#define NORMAL_LOCATION 1
#define TEXCOORDS_LOCATION 2


class ShapeInstance {

public :

    ShapeInstance(const Quad & quad);
        //void fillInstance(const Skybox & skybox);
    ~ShapeInstance();


    ShapeInstance(){
        }

    ShapeInstance& operator=(ShapeInstance const&  InstanceAcopier){
            if(this!=& InstanceAcopier){
                VBO=InstanceAcopier.VBO;
                VAO=InstanceAcopier.VAO;
                vertexCount=InstanceAcopier.vertexCount;
            }
            return *this;
        }

    void draw();

private:
    GLuint VBO;
    GLuint VAO;
    GLsizei vertexCount;

};


#endif //IMACGL_SHAPEINSTANCE_HPP
