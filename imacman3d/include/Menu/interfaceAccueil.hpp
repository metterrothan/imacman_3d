#ifndef _IMAC2GL3_INTERFACEACCUEIL_HPP_
#define _IMAC2GL3_INTERFACEACCUEIL_HPP_

#include "Text.hpp"

class InterfaceAccueil {
   
   private:

      //Etat 0 : interface pas affichée, jeu en route
      //Etat 1 : interface affichée
      //Etat 2 : interface quit
      int etat;


      int nbTexture;

      GLenum id_texture;
      GLuint texture[5];

      GLuint texturePlay;
      Quad btn_Play;
      ShapeInstance instBtn_Play;

      GLuint textureQuit;
      Quad btn_Quit;
      ShapeInstance instBtn_Quit;

      GLuint textureAccueil;
      Quad BackGr_Accueil;
      ShapeInstance instBackGr_Accueil;


   public:

   		InterfaceAccueil(GLenum id);
   		~InterfaceAccueil(){ };

   		void draw();

         int getNbText();
         int getEtat();
         void setEtat(int e);

         void event(SDL_Event &event);
   		
   	
};


#endif