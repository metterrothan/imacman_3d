
#ifndef IMACGL_INTERFACEMANAGER_HPP
#define IMACGL_INTERFACEMANAGER_HPP


#define GLEW_STATIC
#include <iostream>
#include <fstream>
#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include <glm/glm.hpp>
#include "ShapeInstance.hpp"
#include "Text.hpp"

#include "interfaceAccueil.hpp"
//#include "interfacePause.hpp"
//#include "Sound.hpp"

#define NBR_TEXTURES 9

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;


//static const size_t WINDOW_WIDTH = 0, WINDOW_HEIGHT = 0, BYTES_PER_PIXEL = 32;
static const size_t WINDOW_WIDTH = 612, WINDOW_HEIGHT = 612, BYTES_PER_PIXEL = 32;

using namespace std;

// Représente le monde
class InterfaceManager {

    public:
        //contructeur

        InterfaceManager(){

        }

        int run();

    private:

    };


#endif //IMACGL_INTERFACEMANAGER_HPP
