//
// Created by reyes on 12/01/2018.
//
#ifndef IMACGL_SHADER_FCT_HPP
#define IMACGL_SHADER_FCT_HPP

#define GLEW_STATIC
#include <iostream>
#include <fstream>
#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

// Charge, compile et link les fichier sources GLSL passés en paramètre. Renvoit l'identifiant du programme compilé.
GLuint loadProgram(const char* vertexShaderFile, const char* fragmentShaderFile);

#endif //IMACGL_SHADER_FCT_HPP
