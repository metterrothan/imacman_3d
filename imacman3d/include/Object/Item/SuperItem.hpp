#ifndef SUPERITEM_HPP
#define SUPERITEM_HPP

#include "Object/Item/Item.hpp"

class SuperItem : public Item
{
public:
    SuperItem(glm::vec3 pos, Model * object, CustomProgram * program) : Item(0, pos, object, program){ }

    void consume();
};

#endif //SUPERITEM_HPP
