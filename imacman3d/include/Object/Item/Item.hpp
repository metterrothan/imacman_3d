#ifndef ITEM_HPP
#define ITEM_HPP

#include "Object/MapObject.hpp"

class Item : public MapObject
{
protected:
    int m_points = 0;

    /*
    * Callback after the item has been triggered by an event
    * @return void
    */
    virtual void consume();

public:
    Item(int points, glm::vec3 pos, Model * object, CustomProgram * program) : MapObject(pos, object, program){
        m_points = points;

        m_bbox_aabb_width = 0.25f;
        m_bbox_aabb_height = 0.25f;
        m_bbox_aabb_depth = 0.25f;
    }

    void animate();
    void update();

    inline void setPoints(const int points){ m_points = points; }
    inline const int getPoints() const{ return m_points; }
};

#endif //ITEM_HPP
