#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Object/Entity/Entity.hpp"

class EntityAI;

class Player : public Entity
{
private:
    const int MAX_HEALTH = 3;

    int m_score = 0;
    int m_score_temp = 0;
    int m_score_multiplier = 1;
    int m_score_t = -1;

    int m_health = MAX_HEALTH;

    Model * m_death_model;

public:
    Player(const glm::vec3 spawn, Model * object, CustomProgram * program) : Entity(spawn, object, program){
        m_mv_acc_speed = 0.02f;
        m_mv_dec_speed = 0.02f;
        m_mv_max_speed = 0.125f;

        m_mv_max_speed_attack_mod = 0.15f;

        m_bbox_aabb_width = 0.7f;
        m_bbox_aabb_height = 1.0f;
        m_bbox_aabb_depth = 0.7f;

        m_death_model = new Model();
        Model::loadObjFile("stone", m_death_model);
    }

    void update();
    void render(double);
    void keyPressed(const uint32_t, const bool);
    void move();
    void notify(const GameEvent &);

    void isEaten(EntityAI *);
    void doEat(EntityAI *);

    void setDeadState();
    void setHealth(const int);
    void addPoints(const int);

    inline const int getScore() const{ return m_score; }
    inline const int getHealth() const{ return m_health; }
    inline const int getScoreMultiplier() const{ return m_score_multiplier; }
    inline const bool isDead() const{ return m_dead; }
};

#endif //PLAYER_HPP
