#ifndef IMACGL_GHOST_HPP
#define IMACGL_GHOST_HPP

#include "Object/Entity/EntityAI.hpp"

class Shadow : public EntityAI{
public:
    Shadow(const glm::vec3 spawn, Model * object, CustomProgram * program) : EntityAI("Shadow", spawn, object, program){
        m_mv_acc_speed = 0.02f;
        m_mv_dec_speed = 0.02f;
        m_mv_max_speed = 0.095f;
        m_mv_max_speed_attack_mod = m_mv_max_speed * 0.5f;

        m_start_delay = 1000;
    }
};

class Speedy : public EntityAI{
public:
    Speedy(const glm::vec3 spawn, Model * object, CustomProgram * program) : EntityAI("Speedy", spawn, object, program){
        m_mv_acc_speed = 0.03f;
        m_mv_dec_speed = 0.03f;
        m_mv_max_speed = 0.15f;
        m_mv_max_speed_attack_mod = m_mv_max_speed * 0.5f;

        m_start_delay = 2000;
    }
};

class Bashful : public EntityAI{
public:
    Bashful(const glm::vec3 spawn, Model * object, CustomProgram * program) : EntityAI("Bashful", spawn, object, program){
        m_mv_acc_speed = 0.02f;
        m_mv_dec_speed = 0.02f;
        m_mv_max_speed = 0.095f;
        m_mv_max_speed_attack_mod = m_mv_max_speed * 0.5f;

        m_start_delay = 3000;
    }
};

class Pokey : public EntityAI{
public:
    Pokey(const glm::vec3 spawn, Model * object, CustomProgram * program) : EntityAI("Pokey", spawn, object, program){
        m_mv_acc_speed = 0.02f;
        m_mv_dec_speed = 0.02f;
        m_mv_max_speed = 0.07f;
        m_mv_max_speed_attack_mod = m_mv_max_speed * 0.5f;

        m_start_delay = 4000;
    }
};

#endif //IMACGL_GHOST_HPP
