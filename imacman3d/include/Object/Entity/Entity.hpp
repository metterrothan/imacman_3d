#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "Object/MapObject.hpp"
#include "World/Scene.hpp"
#include "World/Map.hpp"
#include "Object/Camera/ViewType.hpp"
#include "Object/Camera/FreeflyCamera.hpp"
#include "Events/GameEvent.hpp"

enum EntityMovement { MV_RIGHT, MV_LEFT, MV_UP, MV_DOWN };

class Entity : public MapObject
{
protected:
    glm::vec3 m_spawn_point;

    float m_mv_acc_speed;
    float m_mv_dec_speed;
    float m_mv_max_speed;

    float m_mv_max_speed_attack_mod;

    bool m_left = false;
    bool m_right = false;
    bool m_up = false;
    bool m_down = false;

    bool m_can_move = true;
    int m_can_move_t = -1;

    // blinking effect
    bool m_blinking = false;
    int m_blinking_speed = 250;
    int m_blinking_t = -1;

    EntityMovement m_mvtype = MV_DOWN;
    float m_speed = 0.0f;

    bool m_blocked_tl = false;
    bool m_blocked_tr = false;
    bool m_blocked_bl = false;
    bool m_blocked_br = false;

    FreeflyCamera * m_camera;

    bool m_dead = false;

public:
    Entity(const glm::vec3 spawn, Model * object, CustomProgram * program) : MapObject(spawn, object, program){
        // starting position
        m_spawn_point = spawn;

        m_camera = new FreeflyCamera();
        m_camera->setFov(30.0f);
        m_camera->setPosition(spawn);
        m_camera->setRotationLeft(0.0f);

        m_mv_acc_speed = 0.02f;
        m_mv_dec_speed = 0.02f;
        m_mv_max_speed = 0.1f;

        m_mv_max_speed_attack_mod = 0.1f;

        m_bbox_aabb_width = 0.6f;
        m_bbox_aabb_height = 1.0f;
        m_bbox_aabb_depth = 0.6f;
    }

    virtual void move();
    void animate();
    virtual void notify(const GameEvent &);

    void update(){
        MapObject::update();

        //restore movement if needed
        if(m_can_move_t != -1 && SDL_GetTicks() >= m_can_move_t){
            m_can_move = !m_can_move;
            m_can_move_t = -1;
        }

        // disable blinking when time is over
        if(m_blinking_t != -1 && SDL_GetTicks() >= m_blinking_t){
            m_blinking = !m_blinking;
            m_blinking_t = -1;
        }
    }

    void setPosition(const float x, const float y, const float z){
        MapObject::setPosition(x, y, z);
        m_camera->setPosition(x, y, z);
    }
    void setPosition(const glm::vec3 & pos){
        MapObject::setPosition(pos);
        m_camera->setPosition(pos);
    }

    inline FreeflyCamera * getCamera() { return m_camera; }

    virtual inline const bool isMoving() const { return (m_left || m_right || m_up || m_down); }

    inline const bool getCanMove() const { return m_can_move; }
    inline const glm::vec3 getSpawn() const { return m_spawn_point; }

    /*
    * Prevents the entity from moving for a certain period of time
    * @param t Duration
    * @return void
    */
    void disableMvt(bool b, const int t = -1){
        m_can_move = b;
        if(m_can_move_t != -1){
            m_can_move_t = SDL_GetTicks() + t;
        }
        else{
            m_can_move_t = -1;
        }
    }

    inline void setCanMove(const bool move){ m_can_move = move; }
    inline void setSpawn(const glm::vec3 spawn){ m_spawn_point = spawn; }

    inline const bool isBliking() const{ return m_blinking; }

    /*
    * Activates/deactivates blinking mode
    * @param b Active or not
    * @param t Duration
    * @return void
    */
    inline void setBlinking(const bool b, const int t = -1){
        m_blinking = b;
        if(t != -1){
            m_blinking_t = SDL_GetTicks() + t;
        }
        else{
            m_blinking_t = -1;
        }
    }

    bool shouldRender(){
        if(m_dead){ return true; }
        return !m_blinking || (int)(SDL_GetTicks() / m_blinking_speed) % 2;
    }
};

#endif //ENTITY_HPP
