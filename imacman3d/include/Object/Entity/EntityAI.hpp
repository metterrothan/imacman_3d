#ifndef ENTITYAI_HPP
#define ENTITYAI_HPP

#include "Object/Entity/Entity.hpp"

class Player;

class EntityAI : public Entity
{
private:
    std::string m_name;
    int m_mvt_randomizer_t;

protected:
    int m_start_delay = 0;

public:
	EntityAI(std::string name, const glm::vec3 spawn, Model * object, CustomProgram * program) : Entity(spawn, object, program){
        m_name = name;

        m_mv_acc_speed = 0.02f;
        m_mv_dec_speed = 0.02f;
        m_mv_max_speed = 0.095f;

        m_bbox_aabb_width = 0.75f;
        m_bbox_aabb_height = 1.0f;
        m_bbox_aabb_depth = 0.75f;
    }

	void update();
	void move();
    void launch();
    void notify(const GameEvent &);

    EntityMovement getRandDir();

	inline const std::string getName() const { return m_name; }
    inline const bool isMoving() const { return true; }
};

#endif //ENTITYAI_HPP
