#ifndef OBJECT_HPP
#define OBJECT_HPP

#define GLEW_STATIC

#include <iostream>
#include <vector>

#include <SDL/SDL.h>
#include <GL/glew.h>
#include <glimac/Image.hpp>
#include <glimac/Program.hpp>
#include <glimac/tiny_obj_loader.h>
#include <glimac/glm.hpp>

#include "Events/GameEvent.hpp"

using namespace glimac;

class Scene;
class CustomProgram;
struct Model;

struct AABB_BBOX{
    float m_w; float m_hw;
    float m_h; float m_hh;
    float m_d; float m_hd;
    glm::vec3 m_pos;
    glm::vec3 m_min;
    glm::vec3 m_max;

    AABB_BBOX(){}
    AABB_BBOX(const glm::vec3 & pos, const float width, const float height, const float depth){
        build(pos, width, height, depth);
    }

    void build(const glm::vec3 & pos, const float width, const float height, const float depth){
        m_pos = pos;

        m_w = width;
        m_h = height;
        m_d = depth;

        // pre-calculate stuff
        m_hw = width / 2;
        m_hh = height / 2;
        m_hd = depth / 2;

        m_min.x = pos.x - width / 2;
        m_max.x = pos.x + width / 2;
        m_min.y = pos.y - height / 2;
        m_max.y = pos.y + height / 2;
        m_min.z = pos.z - depth / 2;
        m_max.z = pos.z + depth / 2;
    }
};

class MapObject
{
protected:
    Scene * m_scene;
    CustomProgram * m_program;
    Model * m_object;

    glm::vec3 m_position, m_old_position;

protected :
    static unsigned int s_cuid;

    const unsigned int UID = s_cuid++;

    // shape
    GLint u_p_matrix;
    GLint u_v_matrix;
    GLint u_m_matrix;
    GLint u_n_matrix;
    GLint u_texture_coord;

    // material
    GLint u_color, u_time;
    GLint u_ka;
    GLint u_kd; // diffuse
    GLint u_ks; // specular
    GLint u_shininess;
    GLint u_alpha;

    GLint u_light1_intensity;
    GLint u_light1_dir; // dl

    GLint u_light2_intensity;
    GLint u_light2_pos; // lp

    glm::mat4 m_m_matrix;
    glm::mat4 m_n_matrix;
    glm::mat4 m_base_matrix;

    float m_bbox_aabb_width = 1.0f;
    float m_bbox_aabb_height = 1.0f;
    float m_bbox_aabb_depth = 1.0f;
    float m_bbox_circle_r = 1.0f;

    bool m_dirty = false;

    AABB_BBOX m_aabb_bbox;

public:
    MapObject(){}
    MapObject(const glm::vec3, Model *, CustomProgram *);

    virtual void init();
    virtual void attach(Scene *);
    virtual void update();
    virtual void render(double);
    virtual void move();
    virtual void animate();
    virtual void notify(const GameEvent &);


    // bounding box stuff
    const AABB_BBOX & getAABBBoundingBox(){
        return m_aabb_bbox;
    }

    static bool intersects(const glm::vec3 & pos, const AABB_BBOX & bbox){
        return !((pos.x < bbox.m_min.x || pos.x > bbox.m_max.x) ||
            (pos.y < bbox.m_min.y || pos.y > bbox.m_max.y) ||
            (pos.z < bbox.m_min.z || pos.z > bbox.m_max.z));
    }

    static bool intersects(const AABB_BBOX & bboxA, const AABB_BBOX & bboxB){
        return (bboxA.m_min.x < bboxB.m_max.x &&
           bboxA.m_max.x > bboxB.m_min.x &&
           bboxA.m_min.y < bboxB.m_max.y &&
           bboxA.m_max.y > bboxB.m_min.y &&
           bboxA.m_min.z < bboxB.m_max.z &&
           bboxA.m_max.z > bboxB.m_min.z);
    }

    virtual void setPosition(const float x, const float y, const float z){
        m_position.x = x;
        m_position.y = y;
        m_position.z = z;
    }
    virtual void setPosition(const glm::vec3 & pos){
        m_position = pos;
    }

    bool operator==(MapObject & o){
        return o.getUID() == UID;
    }

    ~MapObject();

    inline const unsigned int getUID() const { return UID; }
    inline const glm::vec3 & getPosition() const { return m_position; }
    inline const glm::vec3 & getOldPosition() const { return m_old_position; }
    inline const bool isDirty() const{ return m_dirty; }

    inline void setObject(Model * o){ m_object = o; }
    inline void setProgram(CustomProgram * program){ m_program = program; }

    virtual bool shouldRender(){
        return true;
    }
};

#endif //OBJECT_HPP