#ifndef MESH_HPP
#define MESH_HPP

#include <iostream>
#include <vector>

struct Mesh{
    static unsigned int s_cuid;

    const unsigned int UID = s_cuid++;

    std::vector<float> m_vertices;
    std::vector<float> m_normals;
    std::vector<float> m_uvs;
    std::vector<uint32_t> m_indexes;

    Mesh(){

    }

    bool operator==(Mesh & model){
        return model.getUID() == UID;
    }

    inline const unsigned int getUID() const { return UID; }
};

#endif //MESH_HPP
