#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#define GLEW_STATIC

#include <iostream>
#include <unordered_map>

#include <GL/glew.h>
#include <glimac/Image.hpp>
#include <glimac/Program.hpp>
#include <glimac/glm.hpp>
#include "Misc/Utility.hpp"

using namespace glimac;

struct Material
{
    static unsigned int s_cuid;
    static std::unordered_map<std::string, GLuint> s_textures;

    const unsigned int UID = s_cuid++;

    GLuint m_texture;
    glm::vec3 m_color = glm::vec3(0.0f); // backup color

    glm::vec3 m_ka = glm::vec3(0.0f);
    glm::vec3 m_kd = glm::vec3(0.0f);
    glm::vec3 m_ks = glm::vec3(0.0f);
    GLfloat m_shininess = 0.0f;
    GLfloat m_alpha = 1.0f;

    Material(){

    }

    bool operator==(Material & material){
        return material.getUID() == UID;
    }

    static GLuint loadTexture(const std::string);

    inline const unsigned int getUID() const { return UID; }
};

#endif //MATERIAL_HPP
