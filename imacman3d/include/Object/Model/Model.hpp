#ifndef IMACGL_OBJECT_HPP
#define IMACGL_OBJECT_HPP

#include <glimac/tiny_obj_loader.h>

#include "Object/Model/Material.hpp"
#include "Object/Model/Mesh.hpp"

enum ObjectType {
    OBJECT_VOID = 0,
    OBJECT_CUBE = 1,

    OBJECT_BONUS_APPLE = 2,
    OBJECT_BONUS_CHERRY = 3,
    OBJECT_BONUS_PASTEQUE = 4,
    OBJECT_BONUS_PACGUM = 5,
    OBJECT_BONUS_SUPERPACGUM = 6,

    OBJECT_PACMAN = 11,

    OBJECT_GHOST_PINK = 12,
    OBJECT_GHOST_BLUE = 13,
    OBJECT_GHOST_RED = 14,
    OBJECT_GHOST_ORANGE = 15,

    OBJECT_PORTAL = 16,
    OBJECT_DOOR = 17
};

struct Model{
    Material m_material;
    Mesh m_mesh;

    const GLuint VERTEX_ATTR_POSITION = 0;
    const GLuint VERTEX_ATTR_NORMAL = 1;
    const GLuint VERTEX_ATTR_TEX_COORD = 2;

    GLuint m_vbo[3];
    GLuint m_vao;
    GLuint m_ibo;

    Model(){

    }

    void init(){
        glGenBuffers(1, &m_ibo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_mesh.m_indexes.size() * sizeof(uint32_t), m_mesh.m_indexes.data(), GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        // VBO
        glGenVertexArrays(1, &m_vao);
        glGenBuffers(3, m_vbo);

        glBindVertexArray(m_vao);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);

        glBindBuffer(GL_ARRAY_BUFFER, m_vbo[0]);
        glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
        glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
        glBufferData(GL_ARRAY_BUFFER, m_mesh.m_vertices.size() * sizeof(float), m_mesh.m_vertices.data(), GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, m_vbo[1]);
        glEnableVertexAttribArray(VERTEX_ATTR_NORMAL);
        glVertexAttribPointer(VERTEX_ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
        glBufferData(GL_ARRAY_BUFFER, m_mesh.m_normals.size() * sizeof(float), m_mesh.m_normals.data(), GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, m_vbo[2]);
        glEnableVertexAttribArray(VERTEX_ATTR_TEX_COORD);
        glVertexAttribPointer(VERTEX_ATTR_TEX_COORD, 2, GL_FLOAT, GL_FALSE, 0, (void *)0);
        glBufferData(GL_ARRAY_BUFFER, m_mesh.m_uvs.size() * sizeof(float), m_mesh.m_uvs.data(), GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }


    /**
     * Load an object file (.obj)
     * @param filename Used to construct the path to the file in the asset folder
     * @param object Object that is filled with the data
     * @return void
     */
    static void loadObjFile(const std::string & filename, Model * object){
        assert(object != nullptr);
        const std::string objPath = "./assets/models/" + filename + ".obj";

        assert(utility::file_exists(objPath));

        std::vector<tinyobj::shape_t> shapes;
        std::vector<tinyobj::material_t> materials;

        std::string status = tinyobj::LoadObj(shapes, materials, objPath.c_str());

        if(!status.empty()){
            throw std::runtime_error(status);
        }

        object->m_mesh.m_vertices = shapes[0].mesh.positions;
        object->m_mesh.m_normals = shapes[0].mesh.normals;
        object->m_mesh.m_uvs = shapes[0].mesh.texcoords;
        object->m_mesh.m_indexes = shapes[0].mesh.indices;

        loadMaterial(filename, &object->m_material);

        object->init();
    }


    /**
     * Custom material loader
     * Loads a material file (.mtl) from the asset folder + texture (.png)
     * @param filename Filename that is used to construct the path
     * @param material Material object that the data is loaded to
     * @return void
     */
    static void loadMaterial(const std::string & filename, Material * material) throw (const std::runtime_error &){
        const std::string mtlPath = "./assets/materials/" + filename + ".mtl";
        const std::string texturePath = "./assets/textures/" + filename + ".png";

        assert(utility::file_exists(mtlPath));

        if(utility::file_exists(texturePath)){
            material->m_texture = Material::loadTexture(texturePath);
        }

        std::string line;
        std::ifstream file(mtlPath);

        if(!file.is_open()){
            throw std::runtime_error("Could not read mtl file @ " + mtlPath);
        }

        std::string token;
        char delimiter = ' ';

        float shininess = 0.0f;
        glm::vec3 ambient = glm::vec3(0.0f);
        glm::vec3 diffuse = glm::vec3(0.0f);
        glm::vec3 specular = glm::vec3(0.0f);
        float transparency = 1.0f;

        while(getline(file, line)){
            std::stringstream ss(line);
            std::getline(ss, token, delimiter);

            if(token.compare("Tr") == 0){
                std::getline(ss, token, delimiter);
                transparency = strtof(token.c_str(), 0);
            }

            if(token.compare("Ka") == 0){
                std::getline(ss, token, delimiter);
                ambient[0] = strtof(token.c_str(), 0);
                std::getline(ss, token, delimiter);
                ambient[1] = strtof(token.c_str(), 0);
                std::getline(ss, token, delimiter);
                ambient[2] = strtof(token.c_str(), 0);
            }

            else if(token.compare("Kd") == 0){
                std::getline(ss, token, delimiter);
                diffuse[0] = strtof(token.c_str(), 0);
                std::getline(ss, token, delimiter);
                diffuse[1] = strtof(token.c_str(), 0);
                std::getline(ss, token, delimiter);
                diffuse[2] = strtof(token.c_str(), 0);
            }

            else if(token.compare("Ks") == 0){
                std::getline(ss, token, delimiter);
                specular[0] = strtof(token.c_str(), 0);
                std::getline(ss, token, delimiter);
                specular[1] = strtof(token.c_str(), 0);
                std::getline(ss, token, delimiter);
                specular[2] = strtof(token.c_str(), 0);
            }

            else if(token.compare("Ns") == 0){
                std::getline(ss, token, delimiter);
                shininess = strtof(token.c_str(), 0);
            }

        }
        file.close();

        material->m_ka = ambient;
        material->m_kd = diffuse;
        material->m_ks = specular;
        material->m_shininess = shininess;
        material->m_alpha = transparency;
    }

    ~Model(){
        glDeleteBuffers(3, m_vbo);
        glDeleteVertexArrays(1, &m_vao);
    }
};

#endif //IMACGL_OBJECT_HPP
