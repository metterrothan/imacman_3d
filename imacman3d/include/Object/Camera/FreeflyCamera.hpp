#ifndef FREEFLY_CAMERA_HPP
#define FREEFLY_CAMERA_HPP

#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <glimac/glm.hpp>
#include "Camera.hpp"

#define PI 3.1415926535897

class FreeflyCamera : public Camera{
    private:
        float m_fPhi;
        float m_fTheta;

        glm::vec3 m_FrontVector;
        glm::vec3 m_LeftVector;
        glm::vec3 m_UpVector;

    public:
        FreeflyCamera(glm::vec3 pos = glm::vec3(0, 0, 5), float phi = PI, float theta = 0.f){
            m_Position = pos;
            m_fPhi = phi;
            m_fTheta = theta;

            computeDirectionVectors();
        }

        void computeDirectionVectors(){
            m_FrontVector = glm::vec3(cos(m_fTheta)*sin(m_fPhi), sin(m_fTheta), cos(m_fTheta)*cos(m_fPhi));
            m_LeftVector = glm::vec3(sin(m_fPhi + PI / 2), 0, cos(m_fPhi + PI / 2));
            m_UpVector = glm::cross(m_FrontVector, m_LeftVector);
        }

        void moveLeft(float t){
            m_Position += t * m_LeftVector;
        }

        void moveFront(float t){
            m_Position += t * m_FrontVector;
        }

        void rotateLeft(float degrees){
            m_fPhi += glm::radians(degrees);
            computeDirectionVectors();
        }

        void rotateUp(float degrees){
            m_fTheta += glm::radians(degrees);
            computeDirectionVectors();
        }

        void setRotationLeft(float degrees){
            m_fPhi = glm::radians(degrees);
            computeDirectionVectors();
        }
        void setRotationUp(float degrees){
            m_fTheta = glm::radians(degrees);
            computeDirectionVectors();
        }

        glm::mat4 getViewMatrix() const {
            return glm::lookAt(m_Position, m_Position+m_FrontVector, m_UpVector);
        }

        const inline float getLeftRotation() const{ return m_fPhi; }
        const inline float getUpRotation() const{ return m_fTheta; }
};

#endif // FREEFLY_CAMERA_HPP