#ifndef TRACKBALL_CAMERA_HPP
#define TRACKBALL_CAMERA_HPP

#define GLEW_STATIC

#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <glimac/glm.hpp>
#include "Object/Camera/Camera.hpp"

#define PI 3.1415926535897

class TrackballCamera : public Camera{
private:
    float m_fDistance = 0.0f; // distance par rapport au centre de la scène
    float m_fAngleX = 0.0f; // angle caméra axe x
    float m_fAngleY = 0.0f; // angle caméra axe y

    float min_left_clamp = 0.0f;
    float max_left_clamp = 0.0f;
    float min_up_clamp = 0.0f;
    float max_up_clamp = 0.0f;

public:
    TrackballCamera() { }

    inline void setDistance(const float distance){ m_fDistance = distance; }
    inline void setAngleUp(const float angle){ m_fAngleX = angle; }
    inline void setAngleLeft(float angle){ m_fAngleY = angle; }

    inline float getDistance() const{ return m_fDistance; }
    inline float getAngleX() const{ return m_fAngleX; }
    inline float getAngleY() const{ return m_fAngleY; }

    void moveFront(float delta){
        m_fDistance += delta;
    }

    void rotateLeft(float degrees){
        m_fAngleY += degrees;
        if(min_left_clamp != 0 && m_fAngleY < min_left_clamp){ m_fAngleY = min_left_clamp; }
        if(max_left_clamp != 0 && m_fAngleY > max_left_clamp){ m_fAngleY = max_left_clamp; }
    }

    void rotateUp(float degrees){
        m_fAngleX += degrees;
        if(min_up_clamp != 0 && m_fAngleX < min_up_clamp){ m_fAngleX = min_up_clamp; }
        if(min_up_clamp != 0 && m_fAngleX > max_up_clamp){ m_fAngleX = max_up_clamp; }
    }

    void clampUp(float min, float max){
        min_up_clamp = min;
        max_up_clamp = max;
    }
    void clampLeft(float min, float max){
        min_left_clamp = min;
        max_left_clamp = max;
    }

    glm::mat4 getViewMatrix() const {
        glm::mat4 mat = glm::mat4(1.0f);
        mat = glm::translate(mat, glm::vec3(0, 0, m_fDistance));
        mat = glm::rotate(mat, glm::radians(m_fAngleX), glm::vec3(1, 0, 0));
        mat = glm::rotate(mat, glm::radians(m_fAngleY), glm::vec3(0, 1, 0));
        mat = glm::translate(mat, m_Position);
        return mat;
    }
};

#endif //TRACKBALL_CAMERA_HPP

