#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <glimac/glm.hpp>

class Camera{
protected:
    static unsigned int s_cuid;
    const unsigned int UID = s_cuid++;

    glm::vec3 m_Position;
    float m_fov = 45.0f;

public:
    void setPosition(const float x, const float y, const float z){
        m_Position.x = x;
        m_Position.y = y;
        m_Position.z = z;
    }

    virtual glm::mat4 getViewMatrix() const = 0;
    virtual void clampUp(float min, float max){ }
    virtual void clampLeft(float min, float max){ }
    virtual void moveLeft(float t){ }
    virtual void moveFront(float t){ }
    virtual void rotateLeft(float degrees){ }
    virtual void rotateUp(float degrees){ }

    void setPosition(const glm::vec3 & pos){
        m_Position.x = pos.x;
        m_Position.y = pos.y;
        m_Position.z = pos.z;
    }
    inline void setFov(float fov){ m_fov = fov; }

    inline const glm::vec3 getPosition(){ return m_Position; }
    inline const float getFov() const{ return m_fov; }
    inline const unsigned int getUID() const{ return UID; }

    bool operator==(const Camera & camera){
        return camera.getUID() == UID;
    }
};

#endif //CAMERA_HPP
