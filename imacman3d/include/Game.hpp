#ifndef GAME_H
#define GAME_H

#define GLEW_STATIC

#include <GL/glew.h>
#include <iostream>
#include <chrono>
#include <stdexcept>

#include <glimac/SDLWindowManager.hpp>
#include <glimac/FilePath.hpp>

#include "StateFactory.hpp"
#include "GameState/State.hpp"
#include "GameState/MenuState.hpp"
#include "GameState/LevelState.hpp"
#include "GameState/GameState.hpp"
#include "Misc/Utility.hpp"

using namespace glimac;
using namespace std::chrono_literals;
using hrclock = std::chrono::high_resolution_clock;

/**
 * Game
 */
class Game
{
private:
    const int TARGET_UPS = 25; // maximum number of updates per seconds
    const std::chrono::nanoseconds NS_PER_UPDATE = 1000ms / Game::TARGET_UPS;

    std::chrono::nanoseconds m_lag = 0ns;
    hrclock::time_point m_last = hrclock::now();
    hrclock::time_point m_next = hrclock::now() + 1s;

    int m_tmp_fps = 0, m_tmp_ups = 0;
    int m_fps, m_ups;

    std::string m_title;

    int m_ww;
    int m_wh;

    bool m_running = false;

    SDLWindowManager * m_window;
    StateController * m_state_factory;

    static Game * s_instance;

    Game();

public:
    ~Game();

    /**
     * Initializes a game object
     * @param path - Root directory
     * @param w - Width of the window
     * @param h - Height of the window
     * @param title - Title of the window
     */
    void init(const std::string title, const int w, const int h) throw(std::runtime_error);
    void start();
    void stop();
    void run();

    inline const bool isRunning() const { return m_running; }
    inline const int getFPS() const { return m_fps; }
    inline const int getUPS() const { return m_ups; }

    inline const std::string getTitle() const { return m_title; }
    inline const int getWindowWidth() const { return m_ww; }
    inline const int getWindowHeight() const { return m_wh; }

    inline void setTitle(const std::string title){ m_title = title; }
    inline void setWindowWidth(const int w){ m_ww = w; }
    inline void setWindowHeight(const int h){ m_wh = h; }


    static Game * instance(){
        if(!s_instance){
            s_instance = new Game;
        }
        return s_instance;
    }
};

#endif //GAME_H
