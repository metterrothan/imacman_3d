#ifndef TERRAIN_HPP
#define TERRAIN_HPP

#include "Object/MapObject.hpp"
#include "Object/Model/Model.hpp"
#include "Misc/CustomProgram.hpp"

class Terrain : public MapObject{
private:
    GLuint m_attack_tex;
    GLuint m_defense_tex;

public:
    Terrain(const float width, const float height, CustomProgram * program) : MapObject(){
        Model * o = new Model();
        // geo
        o->m_mesh.m_vertices = {
                0.0, 0.0, 0.0,
                0.0, 0.0, height,
                width, 0.0, 0.0,
                width, 0.0, height
        };
        o->m_mesh.m_normals = { 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0 };
        o->m_mesh.m_uvs = { 0, 0, 0, 1, 1, 1, 1, 0 };
        o->m_mesh.m_indexes = { 0, 1, 2, 1, 3, 2 };

        // mat
        o->m_material.m_ks = glm::vec3(0.2);
        o->m_material.m_kd = glm::vec3(0.2);
        o->m_material.m_shininess = 100.0f;
        o->m_material.m_alpha = 0.9f;

        m_attack_tex = Material::loadTexture("./assets/textures/terrain2.png");
        m_defense_tex = Material::loadTexture("./assets/textures/terrain.png");

        o->init();

        setProgram(program);
        setObject(o);
    }

    void update(){
        MapObject::update();
        if(m_scene->getMode() == MODE_DEFENSE){
            m_object->m_material.m_texture = m_defense_tex;
        }
        else{
            m_object->m_material.m_texture = m_attack_tex;
        }
    }
};


#endif //TERRAIN_HPP
