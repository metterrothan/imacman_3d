#ifndef IMACGL_BLOCK_HPP
#define IMACGL_BLOCK_HPP

#include "Object/MapObject.hpp"
#include "World/Scene.hpp"

class Block : public MapObject{
private:
    GLuint m_attack_tex;
    GLuint m_defense_tex;

public:
    Block(const glm::vec3 spawn, Model * object, CustomProgram * program) : MapObject(spawn, object, program){
        m_attack_tex = Material::loadTexture("./assets/textures/cube2.png");
        m_defense_tex = Material::loadTexture("./assets/textures/cube.png");
    }

    void update(){
        MapObject::update();

        // change scene mode
        if(m_scene->getMode() == MODE_DEFENSE){
            m_object->m_material.m_texture = m_defense_tex;
        }
        else{
            m_object->m_material.m_texture = m_attack_tex;
        }
    }
};

#endif //IMACGL_BLOCK_HPP
