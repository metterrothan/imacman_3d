#ifndef SCENE_HPP
#define SCENE_HPP

#define GLEW_STATIC

#include <iostream>
#include <unordered_map>
#include <list>
#include <sstream>
#include <algorithm>

#include <SDL/SDL.h>
#include <GL/glew.h>
#include <glimac/glm.hpp>

#include "Misc/CustomProgram.hpp"
#include "Object/Model/Model.hpp"
#include "Object/Item/Item.hpp"
#include "Object/Camera/ViewType.hpp"
#include "Object/Camera/Camera.hpp"
#include "Object/Camera/TrackballCamera.hpp"
#include "Object/Camera/FreeflyCamera.hpp"
#include "World/SceneMode.hpp"
#include "World/Map.hpp"
#include "Events/GameEvent.hpp"

class MapObject;
class Player;
class Entity;
class EntityAI;
class Skybox;

class Scene
{
private:
    Skybox * m_skybox;
    bool m_ready = false; // démarre la partie

    GLfloat m_aspect_ratio;

    std::unordered_map<ObjectType, Model> m_list;
    CustomProgram m_program;

    Player * m_player;

    SceneMode m_mode = MODE_DEFENSE;
    int m_mode_t = -1;

    std::unordered_map<ViewType, Camera *> m_cameras;
    ViewType m_current_camera = TOP_VIEW;

    glm::mat4 m_proj_matrix;

    Map m_map;
    std::list<MapObject *> m_objects;
    std::list<Item *> m_items;
    std::list<EntityAI *> m_ai_entities;

    glm::vec3 m_door_coord;

    void changeFov(Camera * cam, float);

public:
    void init();
    void update();
    void render(double);
    void keyPressed(const uint32_t, const bool);
    void mouseMove(const glm::ivec2 &);
    void mousePressed(const uint32_t, const bool);
    void loadLevel(const unsigned int);

    /**
    * Broadcast an event to all the entities
    * @param event GameEvent element to broadcast
    * @return void
    */
    void broadcastEvent(const GameEvent & event);

    /*
    * Changes scene mode
    * Affects the way the player can interact with the world
    * @param mode The mode to change to
    * @param t Mode duration, -1 for infinite
    * @return void
    */
    inline void setMode(const SceneMode mode, const int t = -1){
        m_mode = mode;
        if(t != -1) m_mode_t = SDL_GetTicks() + t;
        else m_mode_t = -1;
    }

    const glm::mat4 & getProjMatrix() const{ return m_proj_matrix; }
    const glm::mat4 getViewMatrix();
    inline Player * getPlayer(){ return m_player; }

    inline const Map & getMap() const{ return m_map; }
    const unsigned int getCameraUID();
    inline const ViewType getViewType() const{ return m_current_camera; }
    inline const glm::vec3 getCameraPos(){ return m_cameras[m_current_camera]->getPosition(); }

    inline const glm::vec3 getDoorCoord() const{ return m_door_coord; }

    inline const SceneMode getMode() const{ return m_mode; }

    inline const bool isReady() const{ return m_ready; }

    const std::list<MapObject *> & getObjects() { return m_objects; };
    const std::list<Item *> & getItems() { return m_items; };
    const std::list<EntityAI *> & getAIEntities() { return m_ai_entities; };

    ~Scene();

private:
    void reshape(const int, const int, const GLfloat = 45.0f);

    /*
    * Diffuse an event to all elements of the list
    * @param list List of Map objects
    * @param event
    * @return void
    */
    template <class T> void broadcastToList(const std::list<T *> & list, const GameEvent & event);

    /*
    * Add an object to a list
    * Performs additional redundant tasks on the object before adding it
    * @param list List of Map objects
    * @param object Object that implements update/render methods
    * @return void
    */
    template <class T> void addToList(std::list<T *> & list, T * object);

    /*
     * Updates a batch of objects
     * Loops through all objects of the list and checks if it's marked for deletion BEFORE updating and clean up if needed
     * @param list
     * @return void
     */
    template <class T> void updateList(std::list<T *> & list);

    /*
    * Renders a batch of objects
    * @param list
    * @param delta
    * @return void
    */
    template <class T> void renderList(const std::list<T *> & list, const float delta);

    /*
    * Free a batch of objects
    * @param list
    * @return void
    */
    template <class T> void deleteList(std::list<T *> & list);
};

#endif //SCENE_HPP
