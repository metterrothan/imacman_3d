#ifndef MAP_HPP
#define MAP_HPP

struct Map{
    int m_cols;
    int m_rows;

    ObjectType * m_data;

    inline glm::ivec3 getPosition(const int i) const{
        return glm::ivec3(i / m_cols, 0, i % m_cols);
    }
    inline const ObjectType getIndex(const int col, const int row) const{
        return m_data[row * m_cols + col];
    }
    inline const int size() const{ return m_cols * m_rows; }
    inline const int getCols() const{ return m_cols; }
    inline const int getRows() const{ return m_rows; }

    /**
     * Load a level file (.map) from the asset folder
     * @param level Level index in the folder
     * @param map Level object that the data is loaded to
     * @return void
     */
    static void loadFile(const int level, Map & map) throw (std::runtime_error){
        std::string path = "./assets/levels/level_" + std::to_string(level) + ".map";
        if(!utility::file_exists(path)){
            throw std::runtime_error("Could not find level @ " + path);
        }

        std::string line;
        std::ifstream file(path);

        if(!file.is_open()){
            throw std::runtime_error("Could not read map file @ " + path);
        }

        getline(file, line);
        map.m_rows = std::stoi(line);
        getline(file, line);
        map.m_cols = std::stoi(line);

        map.m_data = new ObjectType[map.m_cols * map.m_rows];

        int row = 0;
        int col = 0;


        std::string token;
        char delimiter = ' ';

        while(getline(file, line)){
            if(line.empty()){
                continue;
            }
            if(row >= map.m_rows){
                throw std::runtime_error("Map - row number does not match");
            }
            col = 0;

            std::stringstream ss(line);
            while(std::getline(ss, token, delimiter)){
                if(token.empty()){
                    continue;
                }
                if(col >= map.m_cols){
                    throw std::runtime_error("Map - col number does not match");
                }
                map.m_data[row * map.m_cols + col] = static_cast<ObjectType>(std::stoi(token));

                col++;
            }
            row++;
        }

        file.close();
    }
};

#endif //MAP_HPP
