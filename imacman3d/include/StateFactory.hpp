#ifndef STATE_FACTORY_H
#define STATE_FACTORY_H

#include <iostream>
#include <map>
#include <stdexcept>
#include <assert.h>

#include "GameState/State.hpp"
#include "Game.hpp"

class State;

class StateController
{
private:
    GameState m_current = LEVEL_STATE;
    std::map<int, State *> m_game_states;

public:
    StateController();
    ~StateController();

    void add(const int, State *);
    void update();
    void render(double);
    void keyPressed(const uint32_t, const bool);
    void mousePressed(const uint32_t, const bool);
    void mouseMove(const glm::ivec2 &, const glm::ivec2 &);
    void setState(GameState);
    void stop();

    inline const GameState getState() const{ return m_current; }
};

#endif //STATE_FACTORY_H
