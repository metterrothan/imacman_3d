const OBJ_PACMAN = 11;

var currentVal = 0;

var tab;
var rows;
var cols;
document.getElementById("list").style.visibility = "hidden";
document.getElementById("lvl").style.visibility = "hidden";
document.getElementById("filename").style.visibility = "hidden";

document.getElementById("grid").addEventListener("contextmenu", function(e){
    e.preventDefault();
    e.stopPropagation();

    let $target = $(e.target);

    let i = parseInt($target.attr("i"));
    let j = parseInt($target.attr("j"));

    let item = getItem(tab[i][j]);

    tab[i][j] = 0;
    $target.removeClass(item);

    return false;
});

$(function(){
    $("#grid").on('click', 'td', function(e){
        e.preventDefault();
        e.stopPropagation();

        var i = parseInt($(this).attr("i"));
        var j = parseInt($(this).attr("j"));

        let val = tab[i][j];
        var obj = getItem(val);

        if(currentVal == OBJ_PACMAN){
            let pacmanItem = getItem(OBJ_PACMAN);
            $("#grid td."+pacmanItem).removeClass(pacmanItem);
        }

        $(this).removeClass(obj);
        $(this).addClass(getItem(currentVal));
        tab[i][j] = currentVal;
        //console.log(currentVal, getItem(currentVal))
    });
});


var list = document.getElementById('list');
$("#list").on("change", function(e){
    e.preventDefault();
    currentVal = parseInt($(this).val());
    //console.log(currentVal);
});

function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();
    document.body.removeChild(element);
}

function initTab(rows, cols){
    tab = new Array(rows);
    for(var i = 0; i < rows; i++){
        tab[i] = new Array(cols);
    }
}

function generate_table() {
    rows = document.getElementById("rows").value;
    cols = document.getElementById("cols").value;
    initTab(rows, cols);
    document.getElementById("generation").style.display = "none";

    document.getElementById("list").style.visibility = "visible";
    document.getElementById("lvl").style.visibility = "visible";
    document.getElementById("filename").style.visibility = "visible";

    var table = document.createElement("table");
    var tbody = document.createElement("tbody");

    for (var i = 0;+ i < rows; i++) {

        var tr = document.createElement("tr");

        for (var j = 0; j < cols; j++) {

            var td = document.createElement("td");
            //var text = document.createTextNode("("+i+","+j+")");
            td.setAttribute("i", i);
            td.setAttribute("j", j);
            tab[i][j] = 0;
            //td.appendChild(text);
            tr.appendChild(td);
            tbody.appendChild(tr);
        }
        table.appendChild(tbody);
        document.getElementById("grid").appendChild(table);

        table.setAttribute("border", "2");

        let wwidth = $("#grid").innerWidth() - 32;
        let wheight = $("#grid").innerHeight() - 32;

        let sw = parseInt(wwidth / cols);
        let sh = parseInt(wheight / rows);

        let tilesize = Math.min(sw, sh);

        $("#grid table td").css({
            width : tilesize+"px",
            height : tilesize+"px"
        });
    }
}

function removeItems(name, val){
    for(var i = 0; i < rows; i++){
        for(var j = 0; j < cols; j++){
            if(tab[i][j] == val){
                $("td."+name).removeClass(name);
                tab[i][j] = 0;
                return;
            }
        }
    }
}

function checkItem(val){
    for(var i = 0; i < rows; i++){
        for(var j = 0; j < cols; j++){
            if(tab[i][j] == val){
                return true;
            }
        }
    }
    return false;
}

function getItem(val){
    var item;
    if(val == 1)
    item = "bloc";
    else if(val == 2)
    item = "pomme";
    else if(val == 3)
    item = "cerise";
    else if(val == 4)
    item = "pasteque";
    else if(val == 5)
    item = "pac-gomme";
    else if(val == 6)
    item = "spac-gomme";
    else if(val == OBJ_PACMAN)
    item = "pacman";
    else if(val == 12)
    item = "fantomeRose";
    else if(val == 13)
    item = "fantomeBleu";
    else if(val == 14)
    item = "fantomeRouge";
    else if(val == 15)
    item = "fantomeOrange";
    else if(val == 17)
    item = "porte";

    return item;
}

function levels() {
    var chaine = "";
    chaine += cols + "\n";
    chaine += rows + "\n";
    for(var i = 0; i < rows; i++){
        if(i != 0)
        chaine += "\n";
        for(var j = 0; j < cols; j++){
            chaine += tab[i][j] + " "
        }
    }

    var filename = document.getElementById("filename").value + ".map";
    if(checkItem(OBJ_PACMAN) == true){
        download(filename, chaine);
    }
    else{
        alert("Halte! Pas d'export possible sans avoir placé Pacman!");
    }
}
