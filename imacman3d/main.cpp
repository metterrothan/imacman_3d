#include <iostream>
#include <glimac/FilePath.hpp>
#include "Game.hpp"
//#include "Menu/interfaceManager.hpp"


using namespace glimac;

int main(int argc, char** argv) {
    FilePath appPath(argv[0]);

    Game * game = Game::instance();
    try{
        game->init("IMACMAN 3D", 1440, 960);
        //InterfaceManager im;
        //im.run();
        game->start();

    }
    catch(const std::exception & e){
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    delete game;

    return EXIT_SUCCESS;
}



