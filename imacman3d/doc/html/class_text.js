var class_text =
[
    [ "Text", "class_text.html#af01b71a930a8b6e413864a8cc08bc1e4", null ],
    [ "~Text", "class_text.html#a2d49e5c280e205125b149f7777ae30c7", null ],
    [ "draw", "class_text.html#adedc069a9ad622bf9d2cf6d194a01b39", null ],
    [ "getIdTexture", "class_text.html#a147008526e868b214587063180fec936", null ],
    [ "getTexture", "class_text.html#afa933fe761195fa254b295e82e1bbe65", null ],
    [ "initTTF", "class_text.html#ae94777297e56ffccfbd3fa2f8339999f", null ],
    [ "setInt", "class_text.html#a7bcd6dd34cbf0ce06befbf155d98e8ce", null ],
    [ "setText", "class_text.html#ab2d8c95b3d746ae3e8c0fba8318743c9", null ]
];