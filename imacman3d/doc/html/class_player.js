var class_player =
[
    [ "Player", "class_player.html#a39e311720bb1b6eaf3afed1ff5a931e1", null ],
    [ "addPoints", "class_player.html#a9f16c11480cf9d0e659fa3186470ed2d", null ],
    [ "doEat", "class_player.html#aafd2cfe51ffac99ef853c40a48e6c8a5", null ],
    [ "getHealth", "class_player.html#a31c302267d1d0857f70842faf3e926e3", null ],
    [ "getScore", "class_player.html#a4e9dfa8f56f823e4e4faa3cf4786601a", null ],
    [ "getScoreMultiplier", "class_player.html#adc2a1330dc8f8ae02735d17388464ec7", null ],
    [ "isEaten", "class_player.html#aac2e82f98f8c48ab0c1a0539f52f9ab7", null ],
    [ "keyPressed", "class_player.html#a9b22684f3228d2366a62143ccf82d88c", null ],
    [ "move", "class_player.html#ae02ee46d8c20dd0697b975f935b09839", null ],
    [ "notify", "class_player.html#a81f8bf3c78e8780ba41d6540ad26e4b8", null ],
    [ "render", "class_player.html#a2f978c7db1ecdc534883d47e1b1fc3ec", null ],
    [ "setDeadState", "class_player.html#a3102dd226e97df987240b362c31a55d5", null ],
    [ "setHealth", "class_player.html#a2f6b575f85ca31d8c72962a99133c3e3", null ],
    [ "update", "class_player.html#a82c3476f3e65a4e2ac6bcd040771bdd4", null ]
];