var class_game =
[
    [ "~Game", "class_game.html#ae3d112ca6e0e55150d2fdbc704474530", null ],
    [ "getFPS", "class_game.html#a2d4326c923cd4fa8dbcd7aade82c4114", null ],
    [ "getTitle", "class_game.html#a57d5b2236373f530f53b4012fc1dedda", null ],
    [ "getUPS", "class_game.html#a2a18478ca7f8fe1f8ff6e71cc1705572", null ],
    [ "getWindowHeight", "class_game.html#a1956720788406351c70c164b91c1ef34", null ],
    [ "getWindowWidth", "class_game.html#adf5449e45841cbb9d7b00e6cb7a579cd", null ],
    [ "init", "class_game.html#a6e735e7f740b9da83b6d8c023f19ce23", null ],
    [ "isRunning", "class_game.html#aa8dae73fb3908851fc0c293e13cc033d", null ],
    [ "run", "class_game.html#a1ab78f5ed0d5ea879157357cf2fb2afa", null ],
    [ "setTitle", "class_game.html#ae26f3440d0d2816194ab234135dc7cd8", null ],
    [ "setWindowHeight", "class_game.html#a8b6eb599c88f3cc10a721a7b99d89d8c", null ],
    [ "setWindowWidth", "class_game.html#af45e4fa99d40932ecc41957278701e6e", null ],
    [ "start", "class_game.html#a3d9b98f7c4a96ecf578f75b96c9f0e90", null ],
    [ "stop", "class_game.html#a17fbb36fd4a2085f9ff4f1fa93d7d08b", null ]
];