var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuv~",
  1: "abcefgilmpqst",
  2: "u",
  3: "bcefgilmpstuv",
  4: "abcdefgiklmnopqrstu~",
  5: "mnpstuv",
  6: "h",
  7: "egosv",
  8: "clmnopt",
  9: "glnpt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Macros"
};

