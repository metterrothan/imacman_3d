var _model_8hpp =
[
    [ "Model", "struct_model.html", "struct_model" ],
    [ "ObjectType", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980", [
      [ "OBJECT_VOID", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980a9e96739ee8f78d86a712aac550a48364", null ],
      [ "OBJECT_CUBE", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980a6115ca1aae9277c8dc737722c52fdb19", null ],
      [ "OBJECT_BONUS_APPLE", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980a72f9d99f52ec0aa84ae69563f37cbb2a", null ],
      [ "OBJECT_BONUS_CHERRY", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980a2989271e2c98f96df8cad75593237869", null ],
      [ "OBJECT_BONUS_PASTEQUE", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980acdfde907e88d5eb11e92f986737b4980", null ],
      [ "OBJECT_BONUS_PACGUM", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980a07f6fb6e118548d5c6b00dbae22126b7", null ],
      [ "OBJECT_BONUS_SUPERPACGUM", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980a42757f85c05bd8057a0be69da1518f17", null ],
      [ "OBJECT_PACMAN", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980a49cf6ea68c52316868a476cca49b8c77", null ],
      [ "OBJECT_GHOST_PINK", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980a6504f011ce36c617bdbfae7d70cad63d", null ],
      [ "OBJECT_GHOST_BLUE", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980a52b1cf693d60574bcfe283e9503755a5", null ],
      [ "OBJECT_GHOST_RED", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980a983b4ec748af0798d1e549145a825c27", null ],
      [ "OBJECT_GHOST_ORANGE", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980a125f54f29a461029de6ced239789690b", null ],
      [ "OBJECT_PORTAL", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980a286515b3f5f38ac092a5989c8c5630f3", null ],
      [ "OBJECT_DOOR", "_model_8hpp.html#a842c5e2e69277690b064bf363c017980a024d286bf64db4a85c09b8a472f9494d", null ]
    ] ]
];