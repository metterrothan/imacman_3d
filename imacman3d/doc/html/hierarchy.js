var hierarchy =
[
    [ "AABB_BBOX", "struct_a_a_b_b___b_b_o_x.html", null ],
    [ "Camera", "class_camera.html", [
      [ "FreeflyCamera", "class_freefly_camera.html", null ],
      [ "TrackballCamera", "class_trackball_camera.html", null ]
    ] ],
    [ "CustomProgram", "struct_custom_program.html", null ],
    [ "Game", "class_game.html", null ],
    [ "ImacShapeVertex", "struct_imac_shape_vertex.html", null ],
    [ "Map", "struct_map.html", null ],
    [ "MapObject", "class_map_object.html", [
      [ "Block", "class_block.html", null ],
      [ "Entity", "class_entity.html", [
        [ "EntityAI", "class_entity_a_i.html", [
          [ "Bashful", "class_bashful.html", null ],
          [ "Pokey", "class_pokey.html", null ],
          [ "Shadow", "class_shadow.html", null ],
          [ "Speedy", "class_speedy.html", null ]
        ] ],
        [ "Player", "class_player.html", null ]
      ] ],
      [ "Item", "class_item.html", [
        [ "SuperItem", "class_super_item.html", null ]
      ] ],
      [ "Terrain", "class_terrain.html", null ]
    ] ],
    [ "Material", "struct_material.html", null ],
    [ "MenuAccueil", "class_menu_accueil.html", null ],
    [ "MenuPause", "class_menu_pause.html", null ],
    [ "Mesh", "struct_mesh.html", null ],
    [ "Model", "struct_model.html", null ],
    [ "Quad", "class_quad.html", null ],
    [ "Scene", "class_scene.html", null ],
    [ "ShapeInstance", "class_shape_instance.html", null ],
    [ "Skybox", "class_skybox.html", null ],
    [ "State", "class_state.html", [
      [ "LevelState", "class_level_state.html", null ],
      [ "MenuState", "class_menu_state.html", null ]
    ] ],
    [ "StateFactory", "class_state_factory.html", null ],
    [ "Text", "class_text.html", null ],
    [ "Timer", "class_timer.html", null ]
];