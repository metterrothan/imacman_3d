var class_timer =
[
    [ "Timer", "class_timer.html#a5f16e8da27d2a5a5242dead46de05d97", null ],
    [ "getDuration", "class_timer.html#a701da9a6ceea3ccb5f1aa014ad75b531", null ],
    [ "pause", "class_timer.html#a0289effad7b573c508bc27e405900a23", null ],
    [ "reset", "class_timer.html#a9020542d73357a4eef512eefaf57524b", null ],
    [ "resume", "class_timer.html#a4ac55a73bb3431db9d4d2fd70ae9a2e8", null ],
    [ "update", "class_timer.html#a745ad59b5a46744cd871a1129a25d74f", null ]
];