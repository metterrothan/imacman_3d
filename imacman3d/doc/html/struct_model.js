var struct_model =
[
    [ "Model", "struct_model.html#ae3b375de5f6df4faf74a95d64748e048", null ],
    [ "~Model", "struct_model.html#ad6ebd2062a0b823db841a0b88baac4c0", null ],
    [ "init", "struct_model.html#a457ca5a6a659cbf6770231c98f2ef760", null ],
    [ "m_ibo", "struct_model.html#a5d7068d151814f339b9533c1c86411bb", null ],
    [ "m_material", "struct_model.html#a9bdd98280096538d7768388925670fc2", null ],
    [ "m_mesh", "struct_model.html#afe425f579d17b798e6c454a5c5e2d952", null ],
    [ "m_vao", "struct_model.html#a9b375dfb121a4343bb07ebfcd085c831", null ],
    [ "m_vbo", "struct_model.html#a8fc18ee458aca7a3fb9dffca39104312", null ],
    [ "VERTEX_ATTR_NORMAL", "struct_model.html#a568c6cf1cb7e8f2b8c3bb8738ebe46bf", null ],
    [ "VERTEX_ATTR_POSITION", "struct_model.html#a2e76f2b08c1c1bbf81673a59fafe70f7", null ],
    [ "VERTEX_ATTR_TEX_COORD", "struct_model.html#a341c773a8253f446334d5b0e6c83bc1d", null ]
];