var struct_material =
[
    [ "Material", "struct_material.html#a137e987401b63eb7c6c27c3e38bc74b5", null ],
    [ "getUID", "struct_material.html#a7080faca3833659f9d795a159693ebdc", null ],
    [ "operator==", "struct_material.html#a77ba206327f0f0b2cdec891caeb838b4", null ],
    [ "m_alpha", "struct_material.html#a6c56392c4baa0d00347227760f00e80f", null ],
    [ "m_color", "struct_material.html#a6efc04cae669ab2bd7b0125e5515df67", null ],
    [ "m_ka", "struct_material.html#ad06da4c1b0227144cd8541bfe46ad18d", null ],
    [ "m_kd", "struct_material.html#a01c662a01435f5e9e3d7c096416e68d3", null ],
    [ "m_ks", "struct_material.html#af634b46e56db3534d7a84d81197691a3", null ],
    [ "m_shininess", "struct_material.html#a84d0d2d6e07d5205a345792d3431f18b", null ],
    [ "m_texture", "struct_material.html#ac648d4f9a3a037699c7428eee9e15612", null ],
    [ "UID", "struct_material.html#aa7709dfca1a6c3fbf97f437205f4e3cb", null ]
];