#version 330 core

out vec4 glFragColor;

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexUV;

out vec3 fFragColor;

uniform float uTime;

uniform sampler2D uTexture;
uniform vec3 uKa;
uniform vec3 uKd;
uniform vec3 uKs;
uniform float uShininess;

uniform float uAlpha;

uniform vec3 uLight1Dir;
uniform vec3 uLight1Intensity;

uniform vec3 uLight2Pos;
uniform vec3 uLight2Intensity;

uniform vec3 uColor;

vec3 blinnPhong(vec3 lInt, vec3 lDir){
	vec3 w0 = normalize(-vPosition);
	vec3 wi = normalize(lDir);

	float dot1 = max(0.0, dot(wi, vNormal));
	float dot2 = max(0.0, dot((w0 + wi)/2, vNormal));

	return lInt * (uKd * dot1 + uKs * pow(dot2, uShininess));
}

vec3 blinnPhong2(vec3 lInt, vec3 lPos){
    vec3 w0 = normalize(lPos-vPosition);
	vec3 wi = normalize(lPos);

	float dot1 = max(0.0, dot(wi, vNormal));
	float dot2 = max(0.0, dot((w0 + wi)/2, vNormal));

    float d = distance(lPos, vPosition);
	return (lInt / (d * d)) * (uKd * dot1 + uKs * pow(dot2, uShininess));
}

vec3 grayscale(vec3 color){
    return vec3(dot(color, vec3(0.299, 0.587, 0.114)));
}


void main(){
    vec3 c = texture(uTexture, vec2(vTexUV.x, 1 - vTexUV.y)).rgb;

    c += blinnPhong(uLight1Intensity, uLight1Dir);
    c += blinnPhong2(uLight2Intensity, uLight2Pos);

    glFragColor = vec4(c, uAlpha);
}