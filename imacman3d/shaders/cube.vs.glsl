#version 330 core

layout(location = 0) in vec3 aVertexPosition;
layout(location = 1) in vec3 aVertexNormal;
layout(location = 2) in vec2 aTexCoord;

uniform mat4 uProjMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uModelMatrix;
uniform mat4 uNormalMatrix;

out vec3 vPosition;
out vec3 vNormal;
out vec2 vTexUV;

void main(){
	gl_Position = uProjMatrix * uViewMatrix * uModelMatrix * vec4(aVertexPosition, 1);
	vPosition = vec3(uViewMatrix * uModelMatrix * vec4(aVertexPosition, 1));
	vNormal = vec3(uNormalMatrix * vec4(aVertexNormal, 1));
	vTexUV = aTexCoord;
}