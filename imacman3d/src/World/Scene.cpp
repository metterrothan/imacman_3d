#include "Game.hpp"
#include "World/Scene.hpp"
#include "World/Block.hpp"
#include "World/Terrain.hpp"
#include "World/Skybox.hpp"
#include "Object/Entity/Player.hpp"
#include "Object/Entity/EntityAI.hpp"
#include "Object/Entity/Ghost.hpp"
#include "Object/Item/SuperItem.hpp"

void Scene::init(){
    reshape(Game::instance()->getWindowWidth(), Game::instance()->getWindowHeight());

    m_cameras[TOP_VIEW] = new TrackballCamera();
    m_cameras[CENTER_VIEW] = new TrackballCamera();
    m_cameras[PACMAN_VIEW] = new FreeflyCamera();

    Model::loadObjFile("pacman", &m_list[OBJECT_PACMAN]);
    Model::loadObjFile("rose", &m_list[OBJECT_GHOST_PINK]);
    Model::loadObjFile("bleu", &m_list[OBJECT_GHOST_BLUE]);
    Model::loadObjFile("rouge", &m_list[OBJECT_GHOST_RED]);
    Model::loadObjFile("orange", &m_list[OBJECT_GHOST_ORANGE]);
    Model::loadObjFile("cube", &m_list[OBJECT_CUBE]);
    Model::loadObjFile("cerise", &m_list[OBJECT_BONUS_CHERRY]);
    Model::loadObjFile("pasteque", &m_list[OBJECT_BONUS_PASTEQUE]);
    Model::loadObjFile("pomme", &m_list[OBJECT_BONUS_APPLE]);
    Model::loadObjFile("gum", &m_list[OBJECT_BONUS_PACGUM]);
    Model::loadObjFile("superGum", &m_list[OBJECT_BONUS_SUPERPACGUM]);
}

void Scene::loadLevel(const unsigned int level){
    m_skybox = new Skybox();

    // Map
    Map::loadFile(level, m_map);

    // Terrain
    float width = m_map.m_cols;
    float height = m_map.m_rows;

    MapObject * test = new Terrain(width, height, &m_program);
    addToList<MapObject>(m_objects, test);

    // objects
    ObjectType type;
    glm::ivec3 coord;

    EntityAI * entity;
    Item * item;
    MapObject * object;
    Block * block;

    for(int row = 0, m = m_map.m_rows; row < m; row++){
        for(int col = 0, n = m_map.m_cols; col < n; col++){
            int i = row * m_map.m_cols + col;
            type = m_map.m_data[i];

            if(type != OBJECT_VOID){
                glm::vec3 pos = glm::vec3(col + 0.5f, 0.5f, row + 0.5f);

                switch(type){
                    case OBJECT_PACMAN :
                        m_player = new Player(pos, &m_list[ type ], &m_program);
                        m_player->init();
                        m_player->attach(this);

                        // PACMAN VIEW positioning
                        m_cameras[PACMAN_VIEW] = m_player->getCamera();
                        break;

                    case OBJECT_GHOST_BLUE:
                        entity = new Bashful(pos, &m_list[ type ], &m_program);
                        addToList<EntityAI>(m_ai_entities, entity);
                        break;

                    case OBJECT_GHOST_PINK:
                        entity = new Speedy(pos, &m_list[ type ], &m_program);
                        addToList<EntityAI>(m_ai_entities, entity);
                        break;

                    case OBJECT_GHOST_ORANGE:
                        entity = new Pokey(pos, &m_list[ type ], &m_program);
                        addToList<EntityAI>(m_ai_entities, entity);
                        break;

                    case OBJECT_GHOST_RED:
                        entity = new Shadow(pos, &m_list[ type ], &m_program);
                        addToList<EntityAI>(m_ai_entities, entity);
                        break;

                    case OBJECT_BONUS_SUPERPACGUM:
                        item = new SuperItem(pos, &m_list[ type ], &m_program);
                        addToList<Item>(m_items, item);
                        break;

                    case OBJECT_BONUS_PACGUM:
                        item = new Item(10, pos, &m_list[ type ], &m_program);
                        addToList<Item>(m_items, item);
                        break;

                    case OBJECT_BONUS_CHERRY:
                    case OBJECT_BONUS_PASTEQUE:
                    case OBJECT_BONUS_APPLE:
                        item = new Item(500, pos, &m_list[ type ], &m_program);
                        addToList<Item>(m_items, item);
                        break;

                    case OBJECT_PORTAL:
                        // TODO : portal
                        break;

                    case OBJECT_DOOR:
                        // TODO : door
                        m_door_coord = pos;
                        break;

                    case OBJECT_CUBE:
                        block = new Block(pos, &m_list[ type ], &m_program);
                        addToList<MapObject>(m_objects, block);
                        break;

                    default:
                        object = new MapObject(pos, &m_list[ type ], &m_program);
                        addToList<MapObject>(m_objects, object);
                        break;
                }
            }
        }
    }

    if(m_player == nullptr){
        throw std::runtime_error("Map - no player found");
    }

    // TOP VIEW positioning
    const glm::vec3 & spawn = m_player->getSpawn();
    m_cameras[TOP_VIEW]->setPosition(-spawn.x, -16.0f, -spawn.z);
    m_cameras[TOP_VIEW]->setFov(55.0f);
    m_cameras[TOP_VIEW]->rotateUp(90.00f);
    m_cameras[TOP_VIEW]->rotateLeft(0.00f);

    m_cameras[CENTER_VIEW]->setPosition(0, -32.0f, -m_map.getRows() / 2);
    m_cameras[CENTER_VIEW]->setFov(45.0f);
    m_cameras[CENTER_VIEW]->rotateUp(75.00f);
    m_cameras[CENTER_VIEW]->rotateLeft(90.00f);
}

void Scene::reshape(const int width, const int height, const GLfloat fov){
    if(width != -1 && height != -1) m_aspect_ratio = (GLfloat)width / height;
    m_proj_matrix = glm::perspective(glm::radians(fov), m_aspect_ratio, 0.1f, 100.f);
}

void Scene::update(){
    if(m_mode_t != -1 && SDL_GetTicks() >= m_mode_t){
        m_mode = MODE_DEFENSE;
    }
    m_player->update();

    updateList(m_objects);
    updateList(m_items);
    updateList(m_ai_entities);

    m_skybox->update();

    if(m_items.size() == 0){
        LOG_MSG("YOU WON!");
    }
}

void Scene::render(double delta){
    // camera follows the player
    const glm::vec3 & pp = m_player->getPosition();
    const glm::vec3 & cp = m_cameras[TOP_VIEW]->getPosition();
    m_cameras[TOP_VIEW]->setPosition(utility::lerp(cp, glm::vec3(-pp.x, cp.y, -pp.z), 0.05));

    m_skybox->render(delta);

    renderList(m_objects, delta);
    renderList(m_items, delta);
    renderList(m_ai_entities, delta);

    m_player->render(delta);
}

void Scene::keyPressed(const uint32_t key, const bool active){
    if(!m_ready && active){
        // start game when the player first hits a key
        m_ready = true;
        broadcastEvent(NOTIFY_READY);
        LOG_MSG("READY");
    }

    m_player->keyPressed(key, active);

    // changing camera mode
    if(active){
        if(key == SDLK_c){
            size_t i = static_cast<size_t>(m_current_camera);
            size_t n = m_cameras.size();

            i++;

            if(i >= n){  i = 0; }
            if(i < 0){ i = n - 1; }

            m_current_camera = static_cast<ViewType>(i);
            reshape(-1, -1, m_cameras[m_current_camera]->getFov());
        }
            // camera zoom
        else if(key == SDLK_PAGEUP){
            changeFov(m_cameras[m_current_camera], -15.0f);
        }
        else if(key == SDLK_PAGEDOWN){
            changeFov(m_cameras[m_current_camera], 15.0f);
        }
    }
}

void Scene::changeFov(Camera * cam, float val){
    float cfov = cam->getFov();
    float fov = cfov + val;

    if(fov < 15){ fov = 60; }
    if(fov > 60){ fov = 15; }

    if(fov != cfov){
        cam->setFov(fov);
        reshape(-1, -1, fov);
    }
}

void Scene::mouseMove(const glm::ivec2 & pos){
    if(m_current_camera == CENTER_VIEW){
        m_cameras[CENTER_VIEW]->rotateUp(-pos.y * 0.075f);
        m_cameras[CENTER_VIEW]->rotateLeft(-pos.x * 0.2f);
    }
}

void Scene::mousePressed(const uint32_t type, const bool active){
    if(type == SDL_BUTTON_RIGHT && active){
        changeFov(m_cameras[m_current_camera], -15.0f);
    }
}


template <class T> void Scene::addToList(std::list<T *> & list, T * object){
    object->init();
    object->attach(this);
    list.push_back(object);
}

template <class T> void Scene::updateList(std::list<T *> & list){
    for(auto it = list.begin(); it != list.end(); ++it){
        if((*it)->isDirty()){
            it = list.erase(it);
            continue;
        }

        (*it)->update();
    }
}

template <class T> void Scene::renderList(const std::list<T *> & list, const float delta){
    for(auto it = list.begin(); it != list.end(); ++it){
        (*it)->render(delta);
    }
}

template <class T> void Scene::deleteList(std::list<T *> & list){
    for(auto it = list.begin(); it != list.end(); ++it){
        delete *it;
    }
    list.clear();
}

template <class T> void Scene::broadcastToList(const std::list<T *> & list, const GameEvent & event){
    for(auto it = list.begin(); it != list.end(); ++it){
        (*it)->notify(event);
    }
}

void Scene::broadcastEvent(const GameEvent & event){
    // TODO add filters to prevent broadcasting to all entities when a few are targeted
    broadcastToList(m_objects, event);
    broadcastToList(m_items, event);
    broadcastToList(m_ai_entities, event);
}

const glm::mat4 Scene::getViewMatrix(){
    return m_cameras[m_current_camera]->getViewMatrix();
}

const unsigned int Scene::getCameraUID(){
    return m_cameras[m_current_camera]->getUID();
}

Scene::~Scene(){
    delete[] m_map.m_data;

    deleteList(m_ai_entities);
    deleteList(m_items);
    deleteList(m_objects);

    for(const auto & temp : m_cameras){
        delete temp.second;
    }

    delete m_player;
}