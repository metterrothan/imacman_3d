#include "StateFactory.hpp"

StateController::StateController(){

}

void StateController::add(const int key, State * state){
    state->init(this);
    m_game_states.insert(std::make_pair(key, state));
}

void StateController::update(){
    assert(m_game_states[m_current] != nullptr);
    m_game_states[m_current]->update();
}

void StateController::render(double delta){
    assert(m_game_states[m_current] != nullptr);
    m_game_states[m_current]->render(delta);
}

void StateController::keyPressed(const uint32_t key, const bool active){
    assert(m_game_states[m_current] != nullptr);
    m_game_states[m_current]->keyPressed(key, active);
}

void StateController::mousePressed(const uint32_t type, const bool active){
    assert(m_game_states[m_current] != nullptr);
    m_game_states[m_current]->mousePressed(type, active);
}

void StateController::mouseMove(const glm::ivec2 & pos, const glm::ivec2 & dir){
    assert(m_game_states[m_current] != nullptr);
    m_game_states[m_current]->setMousePos(pos);
    m_game_states[m_current]->mouseMove(dir);
}

void StateController::setState(GameState index){
    if(index < 0 || index > (int)m_game_states.size() - 1){
        throw std::runtime_error("State index out of bound");
    }
    m_current = index;
}

StateController::~StateController(){
    std::cout << "destroying ... "  << m_game_states.size() << " states" << std::endl;

    for(const auto & temp : m_game_states){
        delete temp.second;
    }
}

void StateController::stop(){
    assert(m_game_states[m_current] != nullptr);
    m_game_states[m_current]->stop();
}