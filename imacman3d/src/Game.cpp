#include "Game.hpp"
//#include "Menu/Menus.hpp"

Game * Game::s_instance = nullptr;

Game::Game(){

}

void Game::init(const std::string title, const int w, const int h) throw(std::runtime_error){
    setTitle(title);
    setWindowWidth(w);
    setWindowHeight(h);

    srand(time(NULL));

    m_state_factory = new StateController();
    // Initialize window
    m_window = new SDLWindowManager((uint32_t)m_ww, (uint32_t)m_wh, m_title.c_str());

    // Initialize GLEW for OpenGL3+ support
    GLenum glewInitError = glewInit();

    if(GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        throw std::runtime_error("Could not initialize GLEW");
    }

   /* if(IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG) == -1){
        std::cerr << "Error IMG init" << std::endl;
        throw std::runtime_error("Could not initialize SDL Image");
    }*/

    std::cout << "OpenGL Version : " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLEW Version : " << glewGetString(GLEW_VERSION) << std::endl;

    // Initialize game state manager
    m_state_factory->add(LEVEL_STATE, new LevelState);
    m_state_factory->add(MENU_STATE, new MenuState);

    m_state_factory->setState(LEVEL_STATE);
}

void Game::start(){
    LOG_MSG("starting...");
    m_running = true;
    run();
}

void Game::stop(){
    LOG_MSG("stopping...");
    m_state_factory->stop();
    m_running = false;
}

void Game::run(){
    glEnable(GL_CULL_FACE);
    glClearColor(0, 0, 0, 0);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_ALWAYS, 0);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    //glEnable(GL_POLYGON_SMOOTH);
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    while(m_running){
        hrclock::time_point now = hrclock::now();
        hrclock::duration elapsed = now - m_last;
        m_last = now;
        m_lag += std::chrono::duration_cast<std::chrono::nanoseconds>(elapsed);

        // Handles input
            SDL_Event e;
            while(m_window->pollEvent(e)){
                if(e.type == SDL_QUIT){
                    stop();
                }
            else if(e.type == SDL_KEYDOWN){
                m_state_factory->keyPressed(e.key.keysym.sym, true);
            }
            else if(e.type == SDL_KEYUP){
                m_state_factory->keyPressed(e.key.keysym.sym, false);
            }
            else if(e.type == SDL_MOUSEBUTTONDOWN){
                m_state_factory->mousePressed(e.button.button, true);
            }
            else if(e.type == SDL_MOUSEBUTTONUP){
                m_state_factory->mousePressed(e.button.button, false);
            }
            else if(e.type == SDL_MOUSEMOTION){
                m_state_factory->mouseMove(glm::ivec2(e.motion.x, e.motion.y), glm::ivec2(e.motion.xrel, e.motion.yrel));
            }
        }

        // Updates
        while(m_lag >= NS_PER_UPDATE){
            m_state_factory->update();
            m_lag -= NS_PER_UPDATE;
            m_tmp_ups++;
        }

        // Rendering
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        m_state_factory->render((double) m_lag.count() / NS_PER_UPDATE.count());
        m_window->swapBuffers();
        m_tmp_fps++;


        // FPS/UPS counter
        if(now >= m_next){
            m_next = now + 1s;

            m_fps = m_tmp_fps; m_tmp_fps = 0;
            m_ups = m_tmp_ups; m_tmp_ups = 0;

            //std::cout << "FPS : " << m_fps << " | " << "UPS : " << m_ups << std::endl;
        }
    }
}

Game::~Game(){
    delete m_state_factory;
    delete m_window;
}