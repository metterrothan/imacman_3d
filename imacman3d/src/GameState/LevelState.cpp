#include "GameState/LevelState.hpp"
#include "World/Scene.hpp"
#include "Object/Entity/Player.hpp"

void LevelState::init(StateController * factory){
    State::init(factory);

    m_scene = new Scene;
    m_scene->init();
    m_scene->loadLevel(m_level);
}

void LevelState::pause(){
    m_paused = true;
    m_timer.pause();
}
void LevelState::resume(){
    m_paused = false;
    m_timer.resume();
}

void LevelState::update(){
    m_timer.update();
    m_scene->update();
}

void LevelState::render(double delta){
    m_scene->render(delta);
}

void LevelState::loadLevel(const unsigned int level){
    if(m_level == level && !m_scene->getPlayer()->isDead()){
        return;
    }

    m_level = level;

    delete m_scene;
    m_scene = new Scene;
    m_scene->init();
    m_scene->loadLevel(level);
}

void LevelState::keyPressed(const uint32_t key, const bool active){
    m_scene->keyPressed(key, active);

    if(active){
        switch(key){
            case SDLK_1: loadLevel(1); break;
            case SDLK_2: loadLevel(2); break;
            case SDLK_3: loadLevel(3); break;
            case SDLK_4: loadLevel(4); break;
            case SDLK_5: loadLevel(5); break;

            case SDLK_ESCAPE :
                 m_controller->setState(MENU_STATE);
                break;

            case SDLK_p :
                isPaused() ? resume() : pause();
                break;

            default: break;
        }
    }
}

void LevelState::mouseMove(const glm::ivec2 & dir){
    if(m_mouse_pressed){
        m_scene->mouseMove(dir);
    }
}

void LevelState::mousePressed(const uint32_t type, const bool active){
    if(type == SDL_BUTTON_LEFT){
        m_mouse_pressed = active;
    }
    m_scene->mousePressed(type, active);
}

void LevelState::stop(){

}

LevelState::~LevelState(){
    delete m_scene;
}