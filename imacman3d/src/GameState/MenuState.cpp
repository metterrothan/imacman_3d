#include "GameState/MenuState.hpp"

void MenuState::init(StateController * factory){
    State::init(factory);

    //glGenTextures(NBR_TEXTURES, m_texture);
    //m_accueil = new MenuAccueil(NBR_TEXTURES + 1 + 5);
}

void MenuState::update(){

}

void MenuState::render(double delta){
    //if(m_accueil->getEtat() == 1) {
        //Activation de passage d'info aux shader

        //glUseProgram(m_program);
        //glUniform1i(glGetUniformLocation(m_program, "uTextureText"), 0);
        //glUniform1i(glGetUniformLocation(m_program, "uText"), true);

        //m_accueil->draw();

        //glUniform1i(glGetUniformLocation(m_program, "uText"), false);
    //}
}

void MenuState::keyPressed(const uint32_t key, const bool active){
    if(active){
        switch(key){
            case SDLK_ESCAPE :
                m_controller->setState(LEVEL_STATE);
                break;

            default: break;
        }
    }
    /*
    std::cout << "Key [" << key << "] " << (active ? "ON" : "OFF") << " (";

    switch(key){
        case SDLK_c : std::cout << "c"; break;
        case SDLK_z : case SDLK_UP : std::cout << "z"; break;
        case SDLK_q : case SDLK_LEFT : std::cout << "q"; break;
        case SDLK_s : case SDLK_DOWN : std::cout << "s"; break;
        case SDLK_d : case SDLK_RIGHT : std::cout << "d"; break;
        case SDLK_SPACE : std::cout << "SPACE"; break;
        case SDLK_RETURN : std::cout << "ENTER"; break;
        default : std::cout << "UNKNOWN"; break;
    }

    std::cout << ")" << std::endl;
    */
}

void MenuState::mousePressed(const uint32_t type, const bool active){
    /*
    std::cout << "Mouse [x:" << m_mouse_pos.x << ", y:" << m_mouse_pos.y << "] " << (active ? "ON" : "OFF") << " (";

    switch(type){
        case SDL_BUTTON_LEFT : std::cout << "LEFT"; break;
        case SDL_BUTTON_MIDDLE : std::cout << "MIDDLE"; break;
        case SDL_BUTTON_RIGHT : std::cout << "RIGHT"; break;
        default : std::cout << "UNKNOWN"; break;
    }

    std::cout << ")"  << std::endl;
    */
}

void MenuState::mouseMove(const glm::ivec2 & dir){

}

void MenuState::stop(){

}

MenuState::~MenuState(){
    /*
    delete m_accueil;
    glDeleteTextures(NBR_TEXTURES, m_texture);
    glDeleteProgram(m_program);
    */
}