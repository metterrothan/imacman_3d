#include "Object/Entity/Entity.hpp"

void Entity::notify(const GameEvent &){

}

void Entity::move(){
    float speed = m_speed;
    float hwidth = m_bbox_aabb_width / 2;//0.35f;
    float hheight = m_bbox_aabb_depth / 2;//0.35f;

    assert(hwidth < 0.5f);
    assert(hheight < 0.5f);

    float tilesize = 1.0f;
    float offsetX = hwidth + 0.01f;
    float offsetZ = hheight + 0.01f;

    float max_speed = (m_scene->getMode() == MODE_ATTACK) ? m_mv_max_speed_attack_mod : m_mv_max_speed;

    m_blocked_tl = false;
    m_blocked_tr = false;
    m_blocked_bl = false;
    m_blocked_br = false;

    // bake speed
    if(m_can_move && isMoving()){
        speed += m_mv_acc_speed;
        if(speed > max_speed){ speed = max_speed; }
    }
    else{
        if(speed > 0){
            speed -= m_mv_dec_speed;
            if(speed < 0){  speed = 0; }
        }
    }

    // collision detection and entity position adjustments
    if(m_mvtype == MV_UP){
        glm::ivec2 c1 = glm::ivec2((int)(m_position.x - hwidth), (int)(m_position.z - hheight - speed));
        glm::ivec2 c2 = glm::ivec2((int)(m_position.x + hwidth), (int)(m_position.z - hheight - speed));

        if(m_scene->getMap().getIndex(c1[0], c1[1]) == OBJECT_CUBE){
            m_blocked_tl = true;
            m_camera->setPosition(m_position.x, m_position.y, c1[1] + tilesize + offsetZ);
            speed = 0;
        }
        if(m_scene->getMap().getIndex(c2[0], c2[1]) == OBJECT_CUBE){
            m_blocked_tr = true;
            m_camera->setPosition(m_position.x, m_position.y, c2[1] + tilesize + offsetZ);
            speed = 0;
        }
    }
    if(m_mvtype == MV_DOWN){
        glm::ivec2 c1 = glm::ivec2((int)(m_position.x - hwidth), (int)(m_position.z + hheight + speed));
        glm::ivec2 c2 = glm::ivec2((int)(m_position.x + hwidth), (int)(m_position.z + hheight + speed));

        if(m_scene->getMap().getIndex(c1[0], c1[1]) == OBJECT_CUBE){
            m_blocked_bl = true;
            m_camera->setPosition(m_position.x, m_position.y, c1[1] - offsetZ);
            speed = 0;
        }
        if(m_scene->getMap().getIndex(c2[0], c2[1]) == OBJECT_CUBE){
            m_blocked_br = true;
            m_camera->setPosition(m_position.x, m_position.y, c2[1] - offsetZ);
            speed = 0;
        }
    }

    if(m_mvtype == MV_LEFT){
        glm::ivec2 c1 = glm::ivec2((int)(m_position.x - hwidth - speed), (int)(m_position.z - hheight));
        glm::ivec2 c2 = glm::ivec2((int)(m_position.x - hwidth - speed), (int)(m_position.z + hheight));

        if(m_scene->getMap().getIndex(c1[0], c1[1]) == OBJECT_CUBE){
            m_blocked_tl = true;
            m_camera->setPosition(c1[0] + tilesize + offsetX, m_position.y, m_position.z);
            speed = 0;
        }
        if(m_scene->getMap().getIndex(c2[0], c2[1]) == OBJECT_CUBE){
            m_blocked_bl = true;
            m_camera->setPosition(c2[0] + tilesize + offsetX, m_position.y, m_position.z);
            speed = 0;
        }
    }
    if(m_mvtype == MV_RIGHT){
        glm::ivec2 c1 = glm::ivec2((int)(m_position.x + hwidth + speed), (int)(m_position.z - hheight));
        glm::ivec2 c2 = glm::ivec2((int)(m_position.x + hwidth + speed), (int)(m_position.z + hheight));

        if(m_scene->getMap().getIndex(c1[0], c1[1]) == OBJECT_CUBE){
            m_blocked_tr = true;
            m_camera->setPosition(c1[0] - offsetX, m_position.y, m_position.z);
            speed = 0;
        }
        if(m_scene->getMap().getIndex(c2[0], c2[1]) == OBJECT_CUBE){
            m_blocked_br = true;
            m_camera->setPosition(c2[0] - offsetX, m_position.y, m_position.z);
            speed = 0;
        }
    }


    if(m_mvtype == MV_UP && m_camera->getLeftRotation() != 180.0f){
        m_camera->setRotationLeft(180.0f);
    }
    else if(m_mvtype == MV_DOWN && m_camera->getLeftRotation() != 0.0f){
        m_camera->setRotationLeft(0.0f);
    }
    else if(m_mvtype == MV_LEFT && m_camera->getLeftRotation() != -90.0f){
        m_camera->setRotationLeft(-90.0f);
    }
    else if(m_mvtype == MV_RIGHT&& m_camera->getLeftRotation() != 90.0f){
        m_camera->setRotationLeft(90.0f);
    }

    // update view position
    m_speed = speed;
    m_camera->moveFront(speed);


    // update model position
    m_position = m_camera->getPosition();
}


void Entity::animate(){
    // make the player model face the current direction
    m_m_matrix = glm::rotate(m_m_matrix, m_camera->getLeftRotation(), glm::vec3(0, 1, 0));
    m_m_matrix = glm::translate(m_m_matrix, glm::vec3(0, sin(SDL_GetTicks() / 1000.0f) * 0.1f, 0));
}