#include "Object/Entity/Player.hpp"
#include "Object/Entity/EntityAI.hpp"

void Player::notify(const GameEvent & event){

}

void Player::isEaten(EntityAI * entity){
    if(isBliking() || m_dead){ // immunity mode | or player is already dead
       return;
    }

    m_score_multiplier = 1; // loose bonus

    setBlinking(true, 3000);
    setHealth(m_health - 1);
    if(!m_dead) setPosition(m_spawn_point);
    disableMvt(true, 1000);

    std::stringstream ss;
    ss << entity->getName() << " ate you !";
    LOG_MSG(ss.str());
}

void Player::doEat(EntityAI * entity){
    if(entity->isBliking() || m_dead){ // immunity mode | or player is already dead
        return;
    }

    entity->setBlinking(true, 2500);
    entity->setPosition(entity->getSpawn());

    if(m_score_t != -1 && SDL_GetTicks() - m_score_t < 2000){
        m_score_multiplier++;
    }
    else{
        m_score_multiplier = 1;
    }
    addPoints(200 * m_score_multiplier);

    std::stringstream ss;
    ss << "Ghost eating bonus x" << m_score_multiplier;
    LOG_MSG(ss.str());

    m_score_t = SDL_GetTicks();
}

void Player::render(double delta){
    if(m_scene->getViewType() != PACMAN_VIEW){
        Entity::render(delta);
    }
}

void Player::update(){
    // blocks the player in 1st person mode
    if(m_can_move_t == -1){
        setCanMove(m_scene->getViewType() == TOP_VIEW);
    }
    Entity::update();
}

void Player::setDeadState(){
    m_dead = true;
    m_object = m_death_model;
    m_speed = 0;
    disableMvt(true, -1);
    setBlinking(false, -1);
}

void Player::move(){
    if(!m_dead){
        Entity::move();
    }
}

void Player::setHealth(const int health){
    m_health = health;

    if(m_health <= 0){
        m_health = 0;

        if(!m_dead){
            setDeadState();
        }
    }
    if(m_health > MAX_HEALTH){
        m_health = MAX_HEALTH;
    }

    std::stringstream ss;
    ss << "Health : "<< m_health << "/" << MAX_HEALTH;
    LOG_MSG(ss.str());
}

void Player::addPoints(const int points){
    m_score += points;
    m_score_temp += points;

    if(m_score_temp >= 10000){
        setHealth(m_health + 1);
        m_score_temp = 0;
    }

    if(points > 0){
        std::stringstream ss;
        ss << "+ "<< points << " points!";
        LOG_MSG(ss.str());
    }
}

void Player::keyPressed(const uint32_t key, const bool active){
    if(m_dead){
        return;
    }

    switch(key){
        case SDLK_w :
        case SDLK_UP :
            m_up = active;

            if(active && m_can_move){
                m_down = false;
                m_mvtype = MV_UP;
            }

            break;

        case SDLK_a :
        case SDLK_LEFT :
            m_left = active;

            if(active && m_can_move){
                m_right = false;
                m_mvtype = MV_LEFT;
            }
            break;

        case SDLK_s :
        case SDLK_DOWN :
            m_down = active;

            if(active && m_can_move){
                m_up = false;
                m_mvtype = MV_DOWN;
            }
            break;

        case SDLK_d :
        case SDLK_RIGHT :
            m_right = active;

            if(active && m_can_move){
                m_left = false;
                m_mvtype = MV_RIGHT;
            }
            break;

        default : break;
    }
}