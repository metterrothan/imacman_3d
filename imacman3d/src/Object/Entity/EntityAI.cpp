#include "Object/Entity/EntityAI.hpp"
#include "Object/Entity/Player.hpp"

void EntityAI::notify(const GameEvent & event){
    switch(event){
        case NOTIFY_READY:
            launch();
            break;

        default: break;
    }
}

void EntityAI::update(){
    Entity::update();

    Player * player = m_scene->getPlayer();

    if(intersects(m_aabb_bbox, player->getAABBBoundingBox())){
        if(m_scene->getMode() == MODE_DEFENSE){
            // normal mode
            player->isEaten(this);
        }
        else if(m_scene->getMode() == MODE_ATTACK){
            // ghost vulnerability mode
            player->doEat(this);
        }
    }
}

void EntityAI::launch(){
    m_mvtype = MV_UP; // face up
    disableMvt(true, m_start_delay); // prevent movement for some time
    m_mvt_randomizer_t = SDL_GetTicks() + 1000; // prevent entity from changing direction for some time
    setPosition(m_scene->getDoorCoord()); // teleport the entity at the entrance
}

void EntityAI::move(){
    Entity::move();

    if(!m_scene->isReady()){
        return; // return if the game is not ready
    }

    if(m_mvtype == MV_UP && (m_blocked_tl || m_blocked_tr)){
        m_mvtype = getRandDir();
    }
    else if(m_mvtype == MV_DOWN && (m_blocked_bl || m_blocked_br)){
        m_mvtype = getRandDir();
    }
    else if(m_mvtype == MV_LEFT && (m_blocked_tl || m_blocked_bl)){
        m_mvtype = getRandDir();
    }
    else if(m_mvtype == MV_RIGHT && (m_blocked_tr || m_blocked_br)){
        m_mvtype = getRandDir();
    }
    else{
        // random movement
        if(SDL_GetTicks() >= m_mvt_randomizer_t){
            m_mvtype = getRandDir();
            m_mvt_randomizer_t = SDL_GetTicks() + utility::random_int(2000, 10000);
        }
    }
}

EntityMovement EntityAI::getRandDir(){
    std::vector<EntityMovement> temp;

    glm::ivec2 left = glm::ivec2((int)(m_position.x - 1.0f), (int)(m_position.z));
    glm::ivec2 right = glm::ivec2((int)(m_position.x + 1.0f), (int)(m_position.z));
    glm::ivec2 up = glm::ivec2((int)(m_position.x), (int)(m_position.z - 1.0f));
    glm::ivec2 down = glm::ivec2((int)(m_position.x), (int)(m_position.z + 1.0f));

    if(m_scene->getMap().getIndex(left[0], left[1]) != OBJECT_CUBE){
        temp.push_back(MV_LEFT);
    }
    if(m_scene->getMap().getIndex(right[0], right[1]) != OBJECT_CUBE){
        temp.push_back(MV_RIGHT);
    }
    if(m_scene->getMap().getIndex(up[0], up[1]) != OBJECT_CUBE){
        temp.push_back(MV_UP);
    }
    if(m_scene->getMap().getIndex(down[0], down[1]) != OBJECT_CUBE){
        temp.push_back(MV_DOWN);
    }


    size_t n = temp.size();

    if(n == 0){
        return m_mvtype;
    }
    if(n == 1){
        return temp[0];
    }

    return temp[utility::random_int(0, n - 1)];
}