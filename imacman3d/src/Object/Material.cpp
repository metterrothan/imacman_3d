#include "Object/Model/Material.hpp"

unsigned int Material::s_cuid = 0;
std::unordered_map<std::string, GLuint> Material::s_textures = {};

GLuint Material::loadTexture(const std::string path){
    assert(utility::file_exists(path));

    // optimization of texture loading, if it already exists we just return the texture unit
    if(s_textures.find(path) != s_textures.end()){
        return s_textures[path];
    }

    GLuint texture;

    std::unique_ptr<Image> m_texture_img = loadImage(path);
    assert(m_texture_img != nullptr);

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_texture_img->getWidth(), m_texture_img->getHeight(), 0, GL_RGBA, GL_FLOAT, m_texture_img->getPixels());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // trilinear filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);


    s_textures[path] = texture;
    return texture;
}