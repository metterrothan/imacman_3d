#include <glimac/tiny_obj_loader.h>
#include "Misc/Utility.hpp"
#include "World/Scene.hpp"
#include "Object/Entity/Player.hpp"

unsigned int MapObject::s_cuid = 0;

MapObject::MapObject(const glm::vec3 pos, Model * object, CustomProgram * program){
    m_position = pos;
    m_object = object;
    m_program = program;
}

void MapObject::attach(Scene * scene){
    assert(scene != nullptr);
    m_scene = scene;
}

void MapObject::init(){
    u_p_matrix = glGetUniformLocation(m_program->getGLId(), "uProjMatrix");
    u_v_matrix = glGetUniformLocation(m_program->getGLId(), "uViewMatrix");
    u_m_matrix = glGetUniformLocation(m_program->getGLId(), "uModelMatrix");

    u_n_matrix = glGetUniformLocation(m_program->getGLId(), "uNormalMatrix");
    u_texture_coord = glGetUniformLocation(m_program->getGLId(), "uTexture");
    u_color = glGetUniformLocation(m_program->getGLId(), "uColor");
    u_alpha = glGetUniformLocation(m_program->getGLId(), "uAlpha");

    u_time = glGetUniformLocation(m_program->getGLId(), "uTime");

    u_kd = glGetUniformLocation(m_program->getGLId(), "uKd");
    u_ks = glGetUniformLocation(m_program->getGLId(), "uKs");
    u_ka = glGetUniformLocation(m_program->getGLId(), "uKa");
    u_shininess = glGetUniformLocation(m_program->getGLId(), "uShininess");

    u_light1_dir = glGetUniformLocation(m_program->getGLId(), "uLight1Dir");
    u_light1_intensity = glGetUniformLocation(m_program->getGLId(), "uLight1Intensity");

    u_light2_pos = glGetUniformLocation(m_program->getGLId(), "uLight2Pos");
    u_light2_intensity = glGetUniformLocation(m_program->getGLId(), "uLight2Intensity");
}

void MapObject::update(){
    m_old_position = m_position;
    move();

    // rebuild the bounding box (in case it changed)
    m_aabb_bbox.build(m_position, m_bbox_aabb_width, m_bbox_aabb_height, m_bbox_aabb_depth);
}

void MapObject::move(){ }

void MapObject::animate(){ }

void MapObject::render(double delta){
    m_m_matrix = glm::translate(m_base_matrix, utility::lerp(m_old_position, m_position, (float)delta));
    animate();
    m_n_matrix = glm::transpose(glm::inverse(m_m_matrix));

    if(shouldRender()){
        glUniformMatrix4fv(u_p_matrix, 1, GL_FALSE, glm::value_ptr(m_scene->getProjMatrix()));
        glUniformMatrix4fv(u_v_matrix, 1, GL_FALSE, glm::value_ptr(m_scene->getViewMatrix()));
        glUniformMatrix4fv(u_m_matrix, 1, GL_FALSE, glm::value_ptr(m_m_matrix));
        glUniformMatrix4fv(u_n_matrix, 1, GL_FALSE, glm::value_ptr(m_n_matrix));

        // material
        glUniform3fv(u_ks, 1, glm::value_ptr(m_object->m_material.m_ks));
        glUniform3fv(u_kd, 1, glm::value_ptr(m_object->m_material.m_kd));
        glUniform3fv(u_ka, 1, glm::value_ptr(m_object->m_material.m_ka));
        glUniform1f(u_shininess, m_object->m_material.m_shininess);
        glUniform3fv(u_color, 1, glm::value_ptr(m_object->m_material.m_color));
        glUniform1f(u_time, SDL_GetTicks() / 10.0f);
        glUniform1f(u_alpha, m_object->m_material.m_alpha);

        // directional light
        glUniform3fv(u_light1_intensity, 1, glm::value_ptr(glm::vec3(0.7000)));
        glUniform3fv(u_light1_dir, 1, glm::value_ptr(  glm::vec4(1.0f) * m_scene->getViewMatrix()));
        // point light
        glUniform3fv(u_light2_intensity, 1, glm::value_ptr(glm::vec3(400.0, 0.0, 0.0)));
        glUniform3fv(u_light2_pos, 1, glm::value_ptr(  glm::vec4(m_scene->getMap().getCols(), 50, m_scene->getMap().getRows(), 1) * m_scene->getViewMatrix() ));

        glBindVertexArray(m_object->m_vao);

        m_program->use();

        glUniform1i(u_texture_coord, 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_object->m_material.m_texture);
        glDrawElements(GL_TRIANGLES, m_object->m_mesh.m_indexes.size(), GL_UNSIGNED_INT, 0);
        glBindTexture(GL_TEXTURE_2D, 0);

        glBindVertexArray(0);
    }
}

void MapObject::notify(const GameEvent &){

}

MapObject::~MapObject(){

}