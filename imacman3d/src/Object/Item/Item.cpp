#include "Object/Item/Item.hpp"
#include "World/Scene.hpp"
#include "Object/Entity/Player.hpp"
#include <algorithm>

void Item::update(){
    MapObject::update();

    // check if the item has collided with the player
    if(intersects(m_position, m_scene->getPlayer()->getAABBBoundingBox())){
        consume();
    }
}

void Item::consume(){
    m_dirty = true; // mark the item has garbage
    m_scene->getPlayer()->addPoints(m_points);
}

void Item::animate(){
    m_m_matrix = glm::rotate(m_m_matrix, SDL_GetTicks() / 2500.0f, glm::vec3(0, 1, 0));
    float offset = (float)sin((m_position.x + m_position.z) / 2 + SDL_GetTicks() / 1000.0f) / 20.0f;
    m_m_matrix = glm::translate(m_m_matrix, glm::vec3(0, offset, 0));
}