#include "Object/Item/SuperItem.hpp"
#include "Object/Entity/EntityAI.hpp"
#include "World/SceneMode.hpp"

void SuperItem::consume(){
    m_dirty = true; // mark the item has garbage

    for(EntityAI * temp : m_scene->getAIEntities()){
        // disable entity movements
        //temp->disableMvt(10000);
        // set attack mode ON for a short period of time
        m_scene->setMode(MODE_ATTACK, 10000);
    }
}