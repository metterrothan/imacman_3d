#include "Menu/ShapeInstance.hpp"


ShapeInstance::ShapeInstance(const Quad & quad)
    {
            // Stockage dans un VBO entrelacé:
            glGenBuffers(1, &VBO);
            glBindBuffer(GL_ARRAY_BUFFER, VBO);

            glBufferData(GL_ARRAY_BUFFER, quad.getByteSize(), quad.getDataPointer(),GL_STATIC_DRAW);
            //glBufferData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(normal_coords) + sizeof(tex_coords), NULL, GL_STATIC_DRAW);


            glBindBuffer(GL_ARRAY_BUFFER, 0);


            // Construction du VAO associé:
            glGenVertexArrays(1, &VAO);
            glBindVertexArray(this->VAO);
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);
            glEnableVertexAttribArray(2);
            glBindBuffer(GL_ARRAY_BUFFER, VBO);

            glVertexAttribPointer(
                    0,
                    3,
                    GL_FLOAT,
                    GL_FALSE,
                    quad.getVertexByteSize(),
                    quad.getPositionOffset()
            );

            glVertexAttribPointer(
                    1,
                    3,
                    GL_FLOAT,
                    GL_FALSE,
                    quad.getVertexByteSize(),
                    quad.getNormalOffset()
            );

            glVertexAttribPointer(
                    2,
                    2,
                    GL_FLOAT,
                    GL_FALSE,
                    quad.getVertexByteSize(),
                    quad.getTexCoordsOffset()
            );
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);


            this->vertexCount=quad.getVertexCount();
    }


ShapeInstance::~ShapeInstance()
    {
            glDeleteBuffers(1,&(this->VAO));
            glDeleteVertexArrays(1,&(this->VBO));
    }


void ShapeInstance::draw()
    {
        glBindVertexArray(this->VAO);
        glDrawArrays(GL_TRIANGLES, 0, this->vertexCount);
        glBindVertexArray(0);
    }


