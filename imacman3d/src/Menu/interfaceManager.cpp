//
// Created by reyes on 12/01/2018.
//
#include "Menu/interfaceManager.hpp"
#include "Menu/Shader_fct.hpp"


int InterfaceManager::run(){


/********************************************************************
* Interface
********************************************************************/

       //InterfacePause pause(NBR_TEXTURES+1);
       InterfaceAccueil accueil(1+5);


/********************************************************************
* Sound
********************************************************************/

        //Sound MySound;
        //MySound.playMusique();

/********************************************************************
* BOUCLE PRINCIPALE
********************************************************************/
        bool done = false;
        while(!done) {

            // Nettoyage de la fenêtre
            glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

            //////////////////////////////////////////////////////////////////
            //Si interface d'accueil affiché
            //////////////////////////////////////////////////////////////////

            if(accueil.getEtat() == 1){
                //Activation de passage d'info aux shader
                GLuint program = loadProgram("shaders/textures.vs.glsl", "shaders/textures.fs.glsl");
                if(!program) {
                    return EXIT_FAILURE;
                }
                glUseProgram(program);
                glUniform1i(glGetUniformLocation(program, "uTextureText"), 0);
                glUniform1i(glGetUniformLocation(program, "uText"), true);
                glUseProgram(program);

                accueil.draw();

                glUniform1i(glGetUniformLocation(program, "uText"), false);

                //pour que l'interface s'affiche


            }  //end if etat == 0
            if(accueil.getEtat() == 2){
                //on quit
                done=true;
            }



            // Mise à jour de l'affichage
            SDL_GL_SwapBuffers();

/********************************************************************
* EVENEMENTS CLAVIER ET SOURIS
********************************************************************/



/********************************************************************
* LIBERATION DE LA MEMOIRE
********************************************************************/

        //Libération des textures
        //glDeleteTextures(NBR_TEXTURES,texture);

        atexit(SDL_Quit);
        return EXIT_SUCCESS;
    }
}
