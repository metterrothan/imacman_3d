#include "Menu/interfaceAccueil.hpp"


	//au débu du jeu : image de fond + deux boutons
	InterfaceAccueil::InterfaceAccueil(GLenum id): 
			btn_Play(1,0.2, 0, 0.0),  instBtn_Play(btn_Play), 
			btn_Quit(1,0.2, 0, -0.4), instBtn_Quit(btn_Quit), 
			BackGr_Accueil(2.1,2.1, 0, 0),  instBackGr_Accueil(BackGr_Accueil) {


		//Etat interface affiché
		this->setEtat(1);

		nbTexture = 5;
		id_texture = id;


		texturePlay = 0;
		textureQuit = 2;
		textureAccueil = 4;


		//Chargement des images
		SDL_Surface *image[5];

		image[0] = IMG_Load("assets/images/bt_jouer.png");
		if (!image[0]){
			std::cerr << "Error loading img jouer" << std::endl;
		}
		image[1] = IMG_Load("assets/images/bt_jouer_roll.png");
		if (!image[1]){
			std::cerr << "Error loading img jouer roll" << std::endl;
		}
		image[2] = IMG_Load("assets/images/bt_quit.png");
		if (!image[2]){
			std::cerr << "Error loading img quit" << std::endl;
		}
		image[3] = IMG_Load("assets/images/bt_quit_roll.png");
		if (!image[3]){
			std::cerr << "Error loading img quit roll" << std::endl;
		}
		image[4] = IMG_Load("assets/images/bg_accueil.png");
		if (!image[4]){
			std::cerr << "Error loading img accueil" << std::endl;
		}

		//std::cout << "toto" << std::endl;

		for (int i=0; i<nbTexture; ++i){
			glBindTexture(GL_TEXTURE_2D, texture[i]);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image[i]->w, image[i]->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image[i]->pixels);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glBindTexture(GL_TEXTURE_2D,0);
		}
		
		//On a plus besoin des images, on libère la mémoire
		SDL_FreeSurface(*image);



	}

	void InterfaceAccueil::draw(){


				glActiveTexture(id_texture+texturePlay);
				glBindTexture(GL_TEXTURE_2D, texture[texturePlay]);
					
					instBtn_Play.draw();

				glActiveTexture(id_texture+textureQuit);
				glBindTexture(GL_TEXTURE_2D, texture[textureQuit]);
					
					instBtn_Quit.draw();

				glActiveTexture(id_texture+textureAccueil);
				glBindTexture(GL_TEXTURE_2D, texture[textureAccueil]);

					instBackGr_Accueil.draw();


				glActiveTexture(id_texture+texturePlay);
				glBindTexture(GL_TEXTURE_2D,0);
				glActiveTexture(id_texture+textureQuit);
				glBindTexture(GL_TEXTURE_2D,0);
				glActiveTexture(id_texture+textureAccueil);
				glBindTexture(GL_TEXTURE_2D,0);
		
	
	}


	int InterfaceAccueil::getNbText(){
		return nbTexture;
	}

	void InterfaceAccueil::setEtat(int e){

		etat = e;
		if(etat != 0) {
			//Interface affiché
			SDL_WM_GrabInput( SDL_GRAB_OFF );
			SDL_ShowCursor(SDL_ENABLE);
		}
			
		else {
			SDL_WM_GrabInput( SDL_GRAB_ON );
			SDL_ShowCursor(SDL_DISABLE);

		}
			
	}
	int InterfaceAccueil::getEtat(){
		return etat;
	}


	void InterfaceAccueil::event(SDL_Event &event){


        switch(event.type) {

            /* souris bouge */
            case SDL_MOUSEMOTION:
                if ( event.button.x > 152 && event.button.x < 456 && event.button.y > 278 && event.button.y < 329 )
                   texturePlay = 1;
            
                else if ( event.button.x > 152 && event.button.x < 456  && event.button.y > 396 && event.button.y < 457)
                   textureQuit = 3;
               	else {
               		texturePlay = 0;
               		textureQuit = 2;
               	}


            break;
            /* souris clic */
            case SDL_MOUSEBUTTONDOWN:

                if ( event.button.x > 152 && event.button.x < 456 && event.button.y > 278 && event.button.y < 329 ) {

                   this->setEtat(0);
                }
            
                else if ( event.button.x > 152 && event.button.x < 456  && event.button.y > 396 && event.button.y < 457)
                   this->setEtat(2);

            break;
        }
    }


