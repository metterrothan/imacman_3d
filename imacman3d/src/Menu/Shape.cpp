#include "Menu/Shape.hpp"

///Constructor
Quad::Quad(GLfloat width, GLfloat height, GLfloat x, GLfloat y) : m_nVertexCount(6) {
    build(width, height, x, y);
}

///Destructeur
Quad::~Quad() {}

void Quad::build(GLfloat width, GLfloat height, GLfloat x, GLfloat y) {

    
    
    // On créé le tableau de vertex (36 vertex * 8 composantes par vertex = 36*8 GLfloat dans le tableau
    // rappel : l'axe des Z pointe vers nous

    GLfloat vertices[6*8] = {
            //face devant 
            // ---------!  Z ! -----------
            -0.5f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
            -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
            0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,

            0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
            0.5f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
            -0.5f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f
    };

    // On remplit chaque vertex du dataPointer avec les valeurs du tableau
    // -> A chaque ShapeVertex du tableau de vertex (m_pDataPointer) correspond une ligne (8 GLfloat) du tableau vertices

    for(GLsizei i = 0; i<6*8; i = i + 8){

        m_pDataPointer[i/8].position.x = vertices[i]*width + x;
        m_pDataPointer[i/8].position.y = vertices[i+1]*height + y;
        m_pDataPointer[i/8].position.z = vertices[i+2];

        m_pDataPointer[i/8].normal.x = vertices[i+3];
        m_pDataPointer[i/8].normal.y = vertices[i+4];
        m_pDataPointer[i/8].normal.z = vertices[i+5];

        m_pDataPointer[i/8].texCoords.x = vertices[i+6];
        m_pDataPointer[i/8].texCoords.y = vertices[i+7];

    }

}




